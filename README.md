# mTaskboard

Frontend for [Taskboard rewrite](https://github.com/kiswa/TaskBoard) written in Mithril

Why?

* For fun
* Because I found the rewrite too slow[*](#footnote1)
* Dragula works too slowly on boards with many tasks; especially if you are using touch devices or Wacom tablets. I replaced it with [mithril-dnd](https://gitlab.com/beelzy/mithril-dnd) for better Mithril compatibility.
* Smaller footprint = faster intial load. Release build scripts currently come up to about ~124KB (3rd party bundles included). I am not including highlightjs because I don't currently see a reason for it yet, and I doubt it will get much bigger if I even added the dashboard mockups.

<a name="footnote1">*</a> This only applies to earlier versions of the re-write before the context menu was rewritten. Then the original is about comparable in speed with this one.

You can plug this into the existing backend of Taskboard. Just run `yarn install` and then `yarn run build` to create the assets in the `dist` folder, and then dump the contents into the dist folder of the original Taskboard. You should only need the files in the `api` folder.

Alternatively, you can attempt to access the backend from a different server. Just replace `Request.DOMAIN` in `src/utils/request.js` with the appropriate url. You are responsible for configuring any CORS or pre-flight issues yourself though. 

For release builds, run `yarn run prod` instead of `yarn run build`.
