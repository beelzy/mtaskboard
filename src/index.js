var m = require('mithril');

var Login = require('./views/login.js');
var Boards = require('./views/boards.js');
var BoardsModel = require('./models/boards.js');
var BoardsModals = require('./views/modal/boards.js');
var Settings = require('./views/settings.js');
var SettingsModel = require('./models/settings.js');
var Notifications = require('./views/notifications.js');
var ContextMenuModel = require('./models/contextmenu.js');
var ContextMenu = require('./views/contextmenu.js');
var Modal = require('./models/modal.js');

m.route(document.body, '/', {
    '/': {
        onmatch: function() {
            BoardsModel.clearBoards();
            Modal.cleanup();
        },
        render: function(vnode) {
            return [m(Login), m(Notifications)];
        }
    },
    '/boards': {
        onmatch: function() {
            ContextMenuModel.init();
            Modal.cleanup();
        },
        render: function(vnode) {
            return [m(Boards), m(Notifications), m(ContextMenu), m(BoardsModals)];
        }
    },
    '/boards/:id': {
        onmatch: function() {
            ContextMenuModel.init();
            Modal.cleanup();
        },
        render: function(vnode) {
            return [m(Boards), m(Notifications), m(ContextMenu), m(BoardsModals)];
        }
    },
    '/settings': {
        onmatch: function() {
            BoardsModel.clearBoards();
            SettingsModel.init();
            Modal.cleanup();
        },
        render: function(vnode) {
            return [m(Settings), m(Notifications)];
        }
    }
});
