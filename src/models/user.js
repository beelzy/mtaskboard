var Request = require('../utils/request.js');
var Strings = require('./strings.js');

var User = {
    user: {},
    users: [],
    options: {},
    updateUser: function(user, options) {
        User.user = user;
        User.user.id = parseInt(User.user.id);
        User.user.security_level = parseInt(User.user.security_level);
        User.user.user_option_id = parseInt(User.user.user_option_id);

        User.updateOptions(options);
        User.replaceUser(user);
    },
    updateOptions: function(options) {
        User.options = options || User.options;

        if (options) {
            Request.strings(User.options.language).then(function(response) {
                Strings.json = response;
            });
        }
    },
    empty: function() {
        return {
            default_board_id: 0,
            email: '',
            id: 0,
            last_login: null,
            security_level: 3,
            user_option_id: 0,
            username: '',
            board_access: [],
            collapsed: [],
            default_board_name: '',
            security_level_name: '',
            can_admin: false
        };
    },
    fromData: function(user) {
        var newUser = {};
        newUser.id = parseInt(user.id);
        newUser.security_level = parseInt(user.security_level);
        newUser.user_option_id = parseInt(user.user_option_id);
        newUser.username = user.username;
        newUser.default_board_id = parseInt(user.default_board_id);
        newUser.email = user.email;
        newUser.last_login = user.last_login;
        newUser.board_access = user.board_access;
        return newUser;
    },
    isAnyAdmin: function() {
        return User.user.security_level === 1 || User.user.security_level === 2;
    },
    replaceUser: function(user) {
        User.users.forEach(function(newUser, index) {
            if (parseInt(user.id) === parseInt(newUser.id)) {
                User.users[index] = User.fromData(newUser);
            }
        });
    }
};

module.exports = User;
