var Notifications = require('./notifications.js');

var UserSettings = {
    changePassword: {
        current: '',
        newPass: '',
        verPass: '',
        submitted: false
    },
    changeUsername: {
        newName: '',
        submitted: false
    },
    changeEmail: {
        newEmail: '',
        submitted: false
    },
    resetPasswordForm: function() {
        UserSettings.changePassword = {
            current: '',
            newPass: '',
            verPass: '',
            submitted: false
        };
    },
    validatePassForm: function() {
        if (UserSettings.changePassword.current === '' || UserSettings.changePassword.newPass === '' || UserSettings.changePassword.verPass === '') {
            Notifications.addNote('error', 'All fields are required to change your password.');

            UserSettings.changePassword.submitted = false;

            return false;
        }

        if (UserSettings.changePassword.newPass !== UserSettings.changePassword.verPass) {
            Notifications.addNote('error', 'New password and verify password do not match.');

            UserSettings.changePassword.submitted = false;

            return false;
        }

        return true;
    },
    resetUsernameForm: function() {
        UserSettings.changeUsername = {
            newName: '',
            submitted: false
        };
    },
    resetEmailForm: function() {
        UserSettings.changeEmail = {
            newEmail: '',
            submitted: false
        };
    }
};

module.exports = UserSettings;
