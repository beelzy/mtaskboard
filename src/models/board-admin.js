var BoardAdmin = {
    userFilter: -1,
    statusFilter: -1,
    sortFilter: 'name-asc',
    loading: true,
    hasBAUsers: false,
    boardToRemove: 0,
    MODAL_ID: 'board-addedit-form',
    MODAL_CONFIRM_ID: 'board-remove-confirm',
    getColor: function(category) {
        if (category.default_task_color) {
            return category.default_task_color;
        }

        return category.defaultColor;
    },
    setColor: function(color, category) {
        if (category.default_task_color) {
            category.default_task_color = color;
        } else {
            category.defaultColor = color;
        }
    }
};

module.exports = BoardAdmin;
