var m = require('mithril');
var Notifications = require('./notifications.js');
var Request = require('../utils/request.js');

var Login = {
    username: '',
    password: '',
    remember: false,
    isSubmitting: false,
    updateUsername: function(e) {
        Login.username = e.target.value;
    },
    updatePassword: function(e) {
        Login.password = e.target.value;
    },
    submit: function(e) {
        e.preventDefault();
        e.stopPropagation();
        if (Login.isSubmitting) {
            return;
        }
        if (Login.username === '' || Login.password === '') {
            Notifications.addNote('error', 'Username and password are required.');
        }
        Login.isSubmitting = true;
        Request.login(Login.username, Login.password, Login.remember).then(function(response) {
            response.alerts.forEach(function(msg) {
                Notifications.addNote(msg.type, msg.text);
            });

            if (response.status === 'success') {
                m.route.set('/boards');
            }
            Login.isSubmitting = false;
        });
    }
};

module.exports = Login;
