var m = require('mithril');
var marked = require('marked');
//var hljs = require('highlightjs'); // Too big; please justify having this

var Notifications = require('./notifications.js');
var Strings = require('./strings.js');

var Task = {
    checkDueDate: function(task) {
        if (task.due_date === '') {
            return;
        }

        var dueDate = new Date(task.due_date);
        
        if (isNaN(dueDate.valueOf())) {
            return;
        }

        var millisecondsPerDay = (1000 * 3600 * 24);
        var today = new Date();
        var timeDiff = today.getTime() - dueDate.getTime();
        var daysDiff = Math.ceil(timeDiff / millisecondsPerDay);

        if (daysDiff > 0) {
            task.isOverDue = true;
        }

        if (daysDiff <= 0 && daysDiff > -3) {
            task.isNearlyDue = true;
        }
    },
    getTextColor: function(color) {
        var r = parseInt(color.substr(1, 2), 16);
        var g = parseInt(color.substr(3, 2), 16);
        var b = parseInt(color.substr(5, 2), 16);
        var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;

        return yiq >= 140 ? '#333333' : '#efefef';
    },
    empty: function() {
        var empty = {};
        empty.id = 0;
        empty.title = '';
        empty.description = '';
        empty.color = '#ffffe0';
        empty.due_date = '';
        empty.points = 0;
        empty.position = 0;
        empty.column_id = 0;
        empty.filtered = false;
        empty.hideFiltered = false;

        empty.comments = [];
        empty.attachments = [];
        empty.assignees = [];
        empty.categories = [];

        return empty;
    },
    calcPercentComplete: function(task) {
        marked['taskCounts'] = [];
        marked['taskCounts'][task.id] = {
            total: 0,
            complete: 0
        };

        var renderer = new marked.Renderer();

        renderer.listitem = function(text) {
            if (/^\s*[[x ]\]\s*/.test(text)) {
                marked['taskCounts'][task.id].total += 1;

                if (/^\s*\[x\]\s*/.test(text)) {
                    marked['taskCounts'][task.id].complete += 1;
                }

                text = text.replace(/^\s*\[ \]\s*/, '<i class="icon icon-check-empty"></i> ')
                .replace(/^\s*\[x\]\s*/, '<i class="icon icon-check"></i> ');

                return '<li class="checklist">' + text + '</li>';
            } else {
                return '<li>' + text + '</li>';
            }
        };

        renderer.link = function(href, title, text) {
            var out = '<a href="' + href + '"';

            if (title) {
                out += ' title="' + title + '"';
            }

            out += ' target="tb_external" rel="noreferrer">' + text + '</a>';

            return out;
        };

        marked.setOptions({
            renderer,
            smartypants: true,
            /*
            highlight: function(code) {
                return hljs.highlightAuto(code).value;
            }*/
        });
        task.percentComplete = 0;

        marked['taskCounts'][task.id] = {
            total: 0,
            complete: 0
        };

        marked(task.description);

        if (marked['taskCounts'][task.id].total) {
            task.percentComplete = marked['taskCounts'][task.id].complete /
                 marked['taskCounts'][task.id].total;
        }
    },
    getPercentTitle: function(task) {
        return Strings.json.boards_task + ' '
            + (task.percentComplete * 100).toFixed(0)
            + '% ' + Strings.json.boards_taskComplete;
    },
    validateTask: function(task) {
        if (task.title === '') {
            Notifications.addNote('error', 'Task title is required.');
            return false;
        }

        return true;
    }
};

module.exports = Task;
