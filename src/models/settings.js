var m = require('mithril');

var Page = require('./page.js');
var Strings = require('./strings.js');
var Actions = require('./actions.js');
var Authenticate = require('../utils/authenticate.js');
var Request = require('../utils/request.js');
var User = require('./user.js');
var Boards = require('./boards.js');
var BoardAdmin = require('./board-admin.js');
var AutoActions = require('./autoactions.js');
var AutoAction = require('./autoaction.js');
var UserSettings = require('./user-settings.js');
var UserAdmin = require('./user-admin.js');
var Notifications = require('./notifications.js');
var Modal = require('./modal.js');

var Settings = {
    boards: [],
    actions: [],
    saving: false,
    init: function() {
        Authenticate.authenticate().then(function(e) {
            Settings.getUsers();
            Page.pagename = Strings.json.settings;
        });
    },
    getUsers: function() {
        Request.getUsers().then(function(response) {
            if (response.data[1]) {
                User.users = [];
                response.data[1].forEach(function(user) {
                    User.users.push(User.fromData(user));
                });
            }

            Settings.getBoards();
        });
    },
    getDefaultBoardName: function(user) {
        var filtered = Settings.boards.filter(function(board) {
            return parseInt(board.id) === user.default_board_id;
        });

        if (filtered.length) {
            return filtered[0].name;
        }

        return Strings.json.none;
    },
    updateUserList: function() {
        BoardAdmin.hasBAUsers = false;

        User.users.forEach(function(user) {
            user.default_board_name = Settings.getDefaultBoardName(user);
            user.security_level_name = parseInt(user.security_level) === 1 ?
                Strings.json.settings_admin :
                parseInt(user.security_level === 2) ?
                Strings.json.settings_boardAdmin :
                Strings.settings_user;

            user.can_admin = true;

            if (user.security_level === 2) {
                BoardAdmin.hasBAUsers = true;
            }

            if (parseInt(user.id) === parseInt(User.user.id) || parseInt(User.user.security_level) === 3) {
                user.can_admin = false;
            }
        });
    },
    getBoards: function() {
        Request.getBoards().then(function(response) {
            var data = response.data[1];

            if (data) {
                Settings.boards = [];
                data.forEach(function(board) {
                    Settings.boards.push(Actions.boardFromData(board));
                });
            }

            Settings.updateUserList();
            Settings.getActions();
            Settings.updateBoardsList();
        });
    },
    getBoard: function(id) {
        var board = null;

        for (var i = 0, size = Settings.boards.length; i < size; ++i) {
            if (parseInt(Settings.boards[i].id) === parseInt(id)) {
                board = Settings.boards[i];
                break;
            }
        }

        return board;
    },
    getBoardName: function(id) {
        var board = Settings.getBoard(id);

        if (board) {
            var note = parseInt(board.is_active) ? '' : '*';

            return board.name + note;
        }

        return '';
    },
    getActions: function() {
        Request.getActions().then(function(response) {
            var data = response.data[1];

            if (data) {
                Settings.actions = data;
                Settings.updateActions();
            }

            UserAdmin.loading = false;
        });
    },
    updateActions: function() {
        Settings.updateHasInactiveBoards();

        Settings.actions.sort(function(a, b) {
            var nameA = Settings.getBoardName(a.board_id);
            var nameB = Settings.getBoardName(b.board_id);

            return nameA.localeCompare(nameB);
        });

        AutoActions.loading = false;
    },
    updateHasInactiveBoards: function() {
        AutoActions.hasInactiveBoards = false;
        Settings.boards.forEach(function(board) {
            if (!parseInt(board.is_active)) {
                AutoActions.hasInactiveBoards = true;
            }
        });
    },
    updateBoardsList: function() {
        Settings.boards.forEach(function(board) {
            Boards.sortBoard(board);
        });

        Settings.filterBoards();
        BoardAdmin.loading = false;
    },
    filterBoards: function() {
        Settings.boards.forEach(function(board) {
            var found = false;

            if (BoardAdmin.userFilter === -1 && BoardAdmin.statusFilter === -1) {
                found = true;
            } else {
                if (BoardAdmin.userFilter > -1) {
                    Settings.boards.users.forEach(function(user) {
                        if (parseInt(user.id) === parseInt(BoardAdmin.userFilter)) {
                            found = true;
                        }
                    });
                }
                if (BoardAdmin.statusFilter > -1) {
                    found = board.is_active && parseInt(BoardAdmin.statusFilter) === 1 || (!board.is_active && parseInt(BoardAdmin.statusFilter === 0));
                }
            }

            if (!found) {
                board.filtered = true;
            } else {
                board.filtered = false;
            }
        });

        Settings.sortBoards();
    },
    sortBoards: function() {
        switch(BoardAdmin.sortFilter) {
            case 'name-asc':
                Settings.boards.sort(function(a, b) {
                   return a.name.localeCompare(b.name);
                });
                break;
            case 'name-desc':
                Settings.boards.sort(function(a, b) {
                    return b.name.localeCompare(a.name);
                });
                break;
            case 'id-desc':
                Settings.boards.sort(function(a, b) {
                    return b.id - a.id;
                });
                break;
            case 'id-asc':
                Settings.boards.sort(function(a, b) {
                    return a.id - b.id;
                });
                break;
        }
    },
    noBoardsMessage: function() {
        return User.user.security_level === 1 ? Strings.json.settings_noBoards : Strings.json.settings_noBoardsMessageUser;
    },
    toggleBoardStatus: function(board) {
        Request.editBoard(Boards.toData(board)).then(function(response) {
            Settings.handleBAResponse(response);
        });
    },
    drop: function() {
        Modal.modalProps.columns.forEach(function(column, index) {
            column.data.position = index;
        });
    },
    addEditBoard: function() {
        var isAdd = Modal.modalProps.title === 'Add';

        Settings.saving = true;
        Settings.setBoardUsers();

        if (!Settings.validateBoard()) {
            Settings.saving = false;
            return;
        }

        if (isAdd) {
            Request.addBoard(Boards.toData(Modal.modalProps)).then(function(response) {
                Settings.handleBAResponse(response);
            });
            return;
        }

        Request.editBoard(Boards.toData(Modal.modalProps)).then(function(response) {
            Settings.handleBAResponse(response);
        });
    },
    removeBoard: function() {
        Settings.saving = true;

        Request.removeBoard(BoardAdmin.boardToRemove).then(function(response) {
            Settings.handleBAResponse(response);

            Settings.getActions();
        });
    },
    setBoardUsers: function() {
        Modal.modalProps.users = [];

        User.users.forEach(function(user) {
            if (user.selected) {
                Modal.modalProps.users.push(user);
            }
        });
    },
    validateBoard: function() {
        if (Modal.modalProps.name === '') {
            Notifications.addNote('error', Strings.json.settings_boardNameError);
            return false;
        }

        if (Modal.modalProps.columns.length === 0) {
            Notifications.addNote('error', Strings.json.settings_columnError);
            return false;
        }

        return true;
    },
    handleBAResponse: function(response) {
        response.alerts.forEach(function(note) {
            Notifications.addNote(note.type, note.text);
        });

        if (response.status === 'success') {
            Modal.close(Modal.modals.editboard);
            Modal.close(Modal.modals.removeboard);

            var boards = [];
            response.data[1].forEach(function(board) {
                boards.push(Actions.boardFromData(board));
            });

            Settings.boards = boards;
            Settings.saving = false;
        }
    },
    handleActionResponse: function(response) {
        response.alerts.forEach(function(note) {
            Notifications.addNote(note.type, note.text);
        });

        Settings.actions = response.data[1];
        Settings.updateActions();
        Settings.saving = false;
        Modal.close(Modal.modals.removeautoaction);
    },
    showModal: function(title, board) {
        Modal.modalProps = Boards.empty(title);

        if (!board) {
            User.users.forEach(function(user) {
                user.selected = false;
            });
        } else {
            Modal.modalProps.id = board.id;
            Modal.modalProps.name = board.name;
            Modal.modalProps.columns = JSON.parse(JSON.stringify(board.columns));
            Modal.modalProps.columns.sort(function(a, b) {
                return a.data.position - b.data.position;
            });
            Modal.modalProps.categories = JSON.parse(JSON.stringify(board.categories));
            Modal.modalProps.issue_trackers = JSON.parse(JSON.stringify(board.issue_trackers));

            User.users.forEach(function(user) {
                var filtered = board.users.filter(function(u) {return parseInt(u.id) === parseInt(user.id);});

                user.selected = filtered.length > 0;
            });
        }

        Modal.open(Modal.modals.editboard);
    },
    showConfirmModal: function(boardid) {
        BoardAdmin.boardToRemove = boardid;
        Modal.open(Modal.modals.removeboard);
    },
    showActionConfirmModal: function(action) {
        AutoActions.actionToRemove = action;
        Modal.open(Modal.modals.removeautoaction);
    },
    noActionsMessage: function() {
        return User.user.security_level < 3 ? Strings.json.settings_noActionsAdmin : Strings.json.settings_noActions;
    },
    getTriggerDescription(action) {
        var desc = '';
        var board = Settings.getBoard(action.board_id);

        if (!board) {
            return;
        }

        switch(parseInt(action.trigger)) {
            case 1:
                desc = Strings.json.settings_triggerMoveToColumn + ' ';
                desc += Settings.getNameFromArray(board.columns, action.source_id);
                break;
            case 2:
                desc = Strings.json.settings_triggerAssignedToUser + ' ';
                desc += Settings.getNameFromArray(board.users, action.source_id, 'username');
                break;
            case 3:
                desc = Strings.json.settings_triggerAddedToCategory + ' ';
                desc += Settings.getNameFromArray(board.categories, action.source_id);
                break;
            case 4:
                desc = Strings.json.settings_triggerPointsChanged;
                break;
        }

        return desc;
    },
    getNameFromArray: function(board, itemid, prop = 'name') {
        var name = '';

        board.forEach(function(item) {
            var isColumn = item.hasOwnProperty('data');
            if (isColumn) {
                if (parseInt(item.data.id) !== parseInt(itemid)) {
                    return;
                }
            } else {
                if (parseInt(item.id) !== parseInt(itemid)) {
                    return;
                }
            }

            name = isColumn ? item.data[prop] : item[prop];
        });
        return name;
    },
    getTypeDescription(action) {
        var desc = '';
        var board = Settings.getBoard(action.board_id);

        if (!board) {
            return;
        }

        switch(parseInt(action.type)) {
            case 1:
                desc = [Strings.json.settings_actionSetColor, m('span', {style: 'background-color: ' + action.change_to + ';'}, action.change_to)];
                break;
            case 2:
                desc = Strings.json.settings_actionSetCategory + ' ';
                desc += Settings.getNameFromArray(board.categories, parseInt(action.change_to));
                break;
            case 3:
                desc = Strings.json.settings_actionAddCategory + ' ';
                desc += Settings.getNameFromArray(board.categories, parseInt(action.change_to));
                break;
            case 4:
                desc = Strings.json.settings_actionSetAssignee + ' ';
                desc += Settings.getNameFromArray(board.users, parseInt(action.change_to), 'username');
                break;
            case 5:
                desc = Strings.json.settings_actionAddAssignee + ' ';
                desc += Settings.getNameFromArray(board.users, parseInt(action.change_to), 'username');
                break;
            case 6:
                desc = Strings.json.settings_actionClearDueDate;
                break;
            case 7:
                desc = Strings.json.settings_actionAlterColor;
                break;
        }

        return desc;
    },
    buildSourcesArray: function(sourceArray, name, arrayName, prop = 'name') {
        AutoActions[sourceArray] = [[null, Strings.json['settings_select' + name]]];

        for (var i = 0, size = Settings.boards.length; i < size; ++i) {
            if (Settings.boards[i].id !== AutoActions.newAction.board_id) {
                continue;
            }

            Settings.boards[i][arrayName].forEach(function(item) {
                var data = [item.id, item[prop]];
                if (arrayName === 'columns') {
                    data = [item.data.id, item.data[prop]];
                }
                AutoActions[sourceArray].push(data);
            });
        }
    },
    updateTriggerSources: function() {
        AutoActions.triggerSources = [];
        AutoActions.newAction.source_id = null;
        AutoActions.newAction.change_to = '#000000';

        switch(AutoActions.newAction.trigger) {
            case 1:
                Settings.buildSourcesArray('triggerSources', 'Column', 'columns');
                break;
            case 2:
                Settings.buildSourcesArray('triggerSources', 'User', 'users', 'username');
                break;
            case 3:
                Settings.buildSourcesArray('triggerSources', 'Category', 'categories');
                break;
        }

        AutoActions.newAction.type = AutoActions.types()[0][0];
        AutoActions.checkAddDisabled();
    },
    updateActionSources: function() {
        AutoActions.actionSources = [];
        AutoActions.newAction.change_to = null;

        switch(AutoActions.newAction.type) {
            case 2:
            case 3:
                Settings.buildSourcesArray('actionSources', 'Category', 'categories');
                break;

            case 4:
            case 5:
                Settings.buildSourcesArray('actionSources', 'Assignee', 'users', 'username');
                break;

            case 1:
                AutoActions.newAction.change_to = '#000000';
                break;
        }

        AutoActions.checkAddDisabled();
    },
    addNewAction: function() {
        Request.addAction(AutoActions.newAction).then(function(response) {
            Settings.handleActionResponse(response);

            AutoActions.newAction = AutoAction.empty();
            AutoActions.checkAddDisabled();
        });
    },
    removeAction: function() {
        Settings.saving = true;

        Request.removeAction(AutoActions.actionToRemove.id).then(function(response) {
            Settings.handleActionResponse(response);
        });
    },
    changePassword: function() {
        UserSettings.changePassword.submitted = true;

        if (!UserSettings.validatePassForm()) {
            return;
        }

        var user = JSON.parse(JSON.stringify(User.user));
        user.new_password = UserSettings.changePassword.newPass;
        user.old_password = UserSettings.changePassword.current;

        Request.editUser(user, User.user.id).then(function(response) {
            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);
            });

            UserSettings.resetPasswordForm();
            UserSettings.changePassword.submitted = false;

            if (response.status === 'success') {
                User.updateUser(JSON.parse(response.data[1]));
            }
        });
    },
    updateUserOptions: function() {
        Request.editUserOptions(User.options, User.user.id).then(function(response) {
            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);
            });

            User.updateOptions(JSON.parse(response.data[1]));
        });
    },
    changeDefaultBoard: function() {
        Request.editUser(User.user, User.user.id).then(function(response) {
            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);
            });

            User.updateUser(JSON.parse(response.data[1]));
        });
    },
    updateUsername: function() {
        UserSettings.changeUsername.submitted = true;

        if (UserSettings.changeUsername.newName === '') {
            Notifications.addNote('error', Strings.json.settings_usernameRequired);
            UserSettings.changeUsername.submitted = false;

            return;
        }

        var user = JSON.parse(JSON.stringify(User.user));
        user.username = UserSettings.changeUsername.newName;

        Request.editUser(user, User.user.id).then(function(response) {
            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);
            });

            UserSettings.resetUsernameForm();
            UserSettings.changeUsername.submitted = false;

            User.updateUser(JSON.parse(response.data[1]));
            Settings.getUsers();
        });
    },
    updateEmail: function() {
        UserSettings.changeEmail.submitted = true;

        var emailRegex = /.+@.+\..+/i;
        var match = UserSettings.changeEmail.newEmail.match(emailRegex);

        if (!match && UserSettings.changeEmail.newEmail !== '') {
            Notifications.addNote('error', 'Invalid email address.');

            UserSettings.changeEmail.submitted = false;

            return;
        }

        var user = JSON.parse(JSON.stringify(User.user));
        user.email = UserSettings.changeEmail.newEmail;

        Request.editUser(user, User.user.id).then(function(response) {
            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);
            });

            UserSettings.resetEmailForm();
            UserSettings.changeEmail.submitted = false;
        });
    },
    addEditUser: function() {
        var isAdd = Modal.modalProps.prefix;
        Settings.saving = true;

        if (!UserAdmin.validateModalUser()) {
            Settings.saving = false;
            return;
        }

        if (isAdd) {
            Request.addUser(Modal.modalProps.user).then(function(response) {
                response.alerts.forEach(function(note) {
                    Notifications.addNote(note.type, note.text);
                });

                Settings.replaceUserList(response);
                Modal.close(Modal.modals.edituser);
                Settings.saving = false;
            });

            return;
        }

        Request.editUser(Modal.modalProps.user, Modal.modalProps.user.id).then(function(response) {
            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);
            });

            User.replaceUser(JSON.parse(response.data[1]));
            Modal.close(Modal.modals.edituser);
            Settings.saving = false;
            Settings.updateUserList();
        });
    },
    removeUser: function() {
        Request.removeUser(UserAdmin.userToRemove).then(function(response) {
            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);
            });

            Settings.replaceUserList(response);

            if (response.status === 'success') {
                Modal.close(Modal.modals.removeuser);
                Settings.updateUserList();
            }
        });
    },
    replaceUserList: function(response) {
        if (response.status === 'success') {
            User.users = [];

            response.data[1].forEach(function(user) {
                User.users.push(User.fromData(user));
            });

            Settings.updateUserList();
        }
    },
    updateBoardAccess: function(e) {
        var selectedOptions = Array.from(e.target.selectedOptions);
        Modal.modalProps.user.boardAccess = [];
        selectedOptions.map(function(option) {
            Modal.modalProps.user.boardAccess.push(option.value);
        });
    }
};

module.exports = Settings;
