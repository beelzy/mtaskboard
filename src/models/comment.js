var User = require('./user.js');

var Comment = {
    canAdminComment: function(comment) {
        if (User.user.id === comment.user_id) {
            return true;
        }

        return User.isAnyAdmin();
    },
    empty: function() {
        return {
            id: 0,
            text: '',
            user_id: 0,
            task_id: 0,
            timestamp: Date.now(),
            is_edited: false
        };
    },
    fromData: function(comment) {
        var newComment = {};
        newComment.id = comment.id;
        newComment.text = comment.text;
        newComment.user_id = parseInt(comment.user_id);
        newComment.task_id = parseInt(comment.task_id);
        newComment.timestamp = parseInt(comment.timestamp);
        newComment.is_edited = comment.is_edited === 1;

        return newComment;
    }
};

module.exports = Comment;
