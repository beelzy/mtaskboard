var Attachment = {
    fromData: function(attachment) {
        var newAttachment = {};
        newAttachment.id = attachment.id;
        newAttachment.filename = attachment.filename;
        newAttachment.name = attachment.name;
        newAttachment.type = attachment.type;
        newAttachment.user_id = parseInt(attachment.user_id);
        newAttachment.timestamp = parseInt(attachment.timestamp);
        newAttachment.task_id = parseInt(attachment.task_id);
        return newAttachment;
    }
};

module.exports = Attachment;
