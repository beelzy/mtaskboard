var Modal = {
    MODAL_ID: 'add-task-form-',
    MODAL_VIEW_ID: 'view-task-form-',
    MODAL_CONFIRM_ID: 'task-remove-confirm',
    MODAL_CONFIRM_COMMENT_ID: 'comment-remove-confirm',
    modals: {
        view: {
            wide: true,
            isOpen: false
        },
        edit: {
            isOpen: false
        },
        removetask: {
            isOpen: false,
            blocking: true
        },
        removecomment: {
            isOpen: false,
            blocking: true
        },
        removeautoaction: {
            isOpen: false,
            blocking: true
        },
        removeboard: {
            isOpen: false,
            blocking: true
        },
        removeuser: {
            isOpen: false,
            blocking: true
        },
        editboard: {
            isOpen: false,
            wide: true
        },
        edituser: {
            isOpen: false
        }
    },
    data: {},
    modalProps: {},
    viewModalProps: {},
    commentEdit: {},
    open: function(modal) {
        modal.isOpen = true;
        document.body.classList.add('no-scroll');
    },
    close: function(modal, checkBlocking) {
        if (checkBlocking && modal.blocking) {
            return;
        }

        modal.isOpen = false;
        document.body.classList.remove('no-scroll');
    },
    cleanup: function() {
        Modal.data = {};
        Modal.modalProps = {};
        Modal.viewModalProps = {};
        Modal.commentEdit = {};
    },
    addColumn: function(e) {
        e.preventDefault();

        if (Modal.modalProps.newColumnName === '') {
            return;
        }

        Modal.modalProps.columns.push({
            data: {
                name: Modal.modalProps.newColumnName,
                position: Modal.modalProps.columns.length
            }
        });
        Modal.modalProps.newColumnName = '';
    },
    removeColumn: function(column) {
        var index = Modal.modalProps.columns.indexOf(column);

        if (index === -1) {
            return;
        }

        Modal.modalProps.columns.splice(index, 1);
    },
    addCategory: function(e) {
        e.preventDefault();

        if (Modal.modalProps.newCategoryName === '') {
            return;
        }

        Modal.modalProps.categories.push({
            name: Modal.modalProps.newCategoryName,
            default_task_color: Modal.modalProps.categoryDefaultColor
        });
    },
    removeCategory: function(category) {
        var index = Modal.modalProps.categories.indexOf(category);

        if (index === -1) {
            return;
        }

        Modal.modalProps.categories.splice(index, 1);
    },
    addIssueTracker: function(e) {
        e.preventDefault();

        if (Modal.modalProps.issueTrackerUrl === '' || Modal.modalProps.issueTrackerBugId === '') {
            return;
        }

        Modal.modalProps.issue_trackers.push({
            url: Modal.modalProps.issueTrackerUrl,
            regex: Modal.modalProps.issueTrackerBugId
        });
        Modal.modalProps.issueTrackerUrl = '';
        Modal.modalProps.issueTrackerBugId = '';
    },
    removeIssueTracker: function(tracker) {
        var index = Modal.modalProps.issue_trackers.indexOf(tracker);

        if (index === -1) {
            return;
        }

        Modal.modalProps.issue_trackers.splice(index, 1);
    }
};

module.exports = Modal;
