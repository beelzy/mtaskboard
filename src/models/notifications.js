var m = require('mithril');

var Notifications = {
    notes: [],
    addNote: function(type, text) {
        var note = {text: text, type: type};
        Notifications.notes.push(note);
        setTimeout(function() {
            var index = Notifications.notes.indexOf(note);
            Notifications.notes.splice(index, 1);
            m.redraw();
        }, 3000);
    },
    removeNote: function(e) {
        var index = e.target.getAttribute('index');
        Notifications.notes.splice(parseInt(index), 1);
    }
};

module.exports = Notifications;
