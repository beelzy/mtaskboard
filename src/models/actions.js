var m = require('mithril');

var Boards = require('./boards.js');
var Column = require('./column.js');
var Task = require('./task.js');
var Modal = require('./modal.js');
var Page = require('./page.js');
var Comment = require('./comment.js');
var Category = require('./category.js');
var AutoAction = require('./autoaction.js');
var Notifications = require('./notifications.js');
var User = require('./user.js');
var Request = require('../utils/request.js');
var Authenticate = require('../utils/authenticate.js');
var Strings = require('./strings.js');

var Actions = {
    boardFromData: function(data) {
        var currentBoard = {};
        currentBoard.id = data.id;
        currentBoard.name = data.name;
        currentBoard.is_active = parseInt(data.is_active);

        currentBoard.columns = [];
        data.ownColumn.forEach(function(column) {
            currentBoard.columns.push(Actions.columnFromData(column, null));
        });

        currentBoard.categories = [];
        if (data.ownCategory) {
            data.ownCategory.forEach(function(category) {
                currentBoard.categories.push(Category.fromData(category));
            });
        }

        currentBoard.auto_actions = [];
        if (data.ownAutoAction) {
            data.ownAutoAction.forEach(function(action) {
                currentBoard.auto_actions.push(AutoAction.fromData(action));
            });
        }

        currentBoard.issue_trackers = [];
        if (data.ownIssueTracker) {
            data.ownIssueTracker.forEach(function(tracker) {
                currentBoard.issue_trackers.push(tracker);
            });
        }

        currentBoard.users = [];
        data.sharedUser.forEach(function(user) {
            currentBoard.users.push(User.fromData(user));
        });

        return currentBoard;
    },
    columnFromData: function(column, old) {
        var newColumn = old || Actions.newColumn();
        newColumn.data = {};
        newColumn.data.id = parseInt(column.id);
        newColumn.data.name = column.name;
        newColumn.data.position = parseInt(column.position);
        newColumn.data.board_id = parseInt(column.board_id);
        newColumn.data.task_limit = parseInt(column.task_limit);

        if (User.user.collapsed) {
            User.user.collapsed.forEach(function(id) {
                if (parseInt(id) === parseInt(newColumn.data.id)) {
                    newColumn.isCollapsed = true;
                }
            });
        }

        newColumn.data.tasks = [];
        if (column.hasOwnProperty('ownTask')) {
            column.ownTask.forEach(function(task) {
                newColumn.data.tasks.push(Actions.taskFromData(task));
            });
        }

        Column.sortTasks(newColumn);
        return newColumn;
    },
    newColumn: function() {
        var column = {
            collapseTasks: false,
            sortOption: 'pos',
            showLimitEditor: false,
            collapseActivity: false,
            showActivity: false,
            isCollapsed: false,
            newlimit: 0,
            viewTaskActivities: [],
            data: {},
            quickAdd: Task.empty()
        };

        return column;
    },
    taskFromData: function(task) {
        var newTask = Task.empty();
        newTask.id = parseInt(task.id);
        newTask.title = task.title;
        newTask.description = task.description;
        newTask.color = task.color;
        newTask.due_date = task.due_date;
        newTask.points = parseInt(task.points);
        newTask.position = parseInt(task.position);
        newTask.column_id = parseInt(task.column_id);
        newTask.filtered = false;
        newTask.hideFiltered = false;

        var comments = task.ownComment || task.comments || [];

        comments.forEach(function(comment) {
            newTask.comments.push(Comment.fromData(comment));
        });

        var attachments = task.ownAttachment || task.attachments || [];

        attachments.forEach(function(attachment) {
            newTask.attachments.push(Attachment.fromData(attachment));
        });

        var users = task.sharedUser || task.assignees || [];

        users.forEach(function(user) {
            newTask.assignees.push(User.fromData(user));
        });

        var categories = task.sharedCategory || task.categories || [];

        categories.forEach(function(category) {
            newTask.categories.push(Category.fromData(category));
        });

        Task.checkDueDate(newTask);
        // I don't know what this is for
        //Task.calcPercentComplete(newTask);

        return newTask;
    },
    updateActiveBoard: function(boardData) {
        var newboard = Actions.boardFromData(boardData);
        Boards.sortBoard(newboard);
        Boards.activeboard = newboard;
    },
    checkAuthentication: function(isinit) {
        if (Boards.boardNavId !== m.route.param('id') && !Boards.loading) {
            Boards.showBoard = false;
            Boards.loading = true;
            Authenticate.authenticate().then(function(e) {
                Boards.boardNavId = m.route.param('id');
                if (isinit) {
                    Actions.getBoards();
                }
                if (!Boards.boardNavId && User.user && User.user.default_board_id) {
                    Boards.goToBoard(User.user.default_board_id);
                } else {
                    Boards.setActiveBoard();
                }
            }).finally(function() {
                Boards.showBoard = true;
                document.title = 'mTaskboard - Kanban App';
                Boards.loading = false;
            });
        }
    },
    getBoards: function() {
        Page.pagename = Boards.activeboard ? Page.pagename : Strings.json.boards;
        Request.getBoards().then(function(response) {
            var data = response.data[1];

            if (data) {
                Boards.boards = [];
                data.forEach(function(board) {
                    var currentBoard = Actions.boardFromData(board);

                    if (currentBoard.is_active) {
                        Boards.boards.push(currentBoard);
                    }

                });

                Boards.sortBoards();
                Boards.setActiveBoard();
            }
        });       
    },
    showModal: function(data, task) {
        Modal.data = data;
        if (!task) {
            Modal.modalProps = Task.empty();
            Modal.modalProps.column_id = data.data.id;

            Modal.open(Modal.modals.edit);
            return;
        }

        Modal.modalProps = Actions.taskFromData(task);
        Modal.modalProps.assignees = [];
        Modal.modalProps.categories = [];

        Boards.activeboard.users.forEach(function(user) {
            task.assignees.forEach(function(assignee) {
                if (assignee.id === user.id) {
                    Modal.modalProps.assignees.push(user);
                }
            });
        });

        Boards.activeboard.categories.forEach(function(category) {
            task.categories.forEach(function(cat) {
                if (cat.id === category.id) {
                    Modal.modalProps.categories.push(category);
                }
            });
        });

        Modal.open(Modal.modals.edit);
    },
    showViewModal: function(data, task) {
        data.viewTaskActivities = [];
        Request.getTaskActivity(task.id).then(function(response) {
            response.data[1].forEach(function(item) {
                data.viewTaskActivities.push(item);
            });
        });

        Modal.newComment = '';
        Modal.viewModalProps = Actions.taskFromData(task);

        Task.checkDueDate(Modal.viewModalProps);
        data.showActivity = false;
        Modal.data = data;
        setTimeout(function() {data.showActivity = true; m.redraw();}, 500);
        Modal.open(Modal.modals.view);
    },
    editComment: function() {
        Modal.commentEdit.is_edited = true;
        Modal.commentEdit.user_id = User.user.id;

        Request.updateComment(Modal.commentEdit).then(function(response) {
            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);

                if (response.status !== 'success') {
                    return;
                }

                var updatedTask = response.data[1][0];
                Actions.replaceUpdatedTask(updatedTask);
            });
        });
    },
    addComment: function() {
        if (Modal.viewModalProps.id < 1) {
            return;
        }

        var newComment = Comment.empty();
        newComment.text = Modal.newComment;
        newComment.user_id = User.user.id;
        newComment.task_id = Modal.viewModalProps.id;

        Modal.viewModalProps.comments.push(newComment);

        Modal.newComment = '';

        Request.updateTask(Modal.viewModalProps).then(function(response) {
            if (response.status !== 'success') {
                return;
            }

            var updatedTask = response.data[1][0];
            Actions.replaceUpdatedTask(updatedTask);

            Modal.viewModalProps = Actions.taskFromData(updatedTask);
        });
    },
    removeComment: function() {
        for (var i = Modal.viewModalProps.comments.length - 1; i >= 0; --i) {
            if (Modal.viewModalProps.comments[i].id === Modal.commentToRemove.id) {
                Modal.viewModalProps.comments.splice(i, 1);
            }
        }

        Request.removeComment(Modal.commentToRemove.id).then(function(response) {
            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);
            });
            if (response.status !== 'success') {
                return;
            }

            var updatedTask = response.data[1][0];
            Actions.replaceUpdatedTask(updatedTask);
        });
    },
    addTask: function(task) {
        Boards.saving = true;

        if (!Task.validateTask(task)) {
            Boards.saving = false;
            return;
        }

        Request.addTask(task).then(function(response) {
            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);
            });

            if (response.status !== 'success') {
                return;
            }

            var boardData = response.data[2][0];

            boardData.ownColumn.forEach(function(column) {
                if (!column.ownTask) {
                    column.ownTask = [];
                }
            });

            Actions.updateActiveBoard(boardData);
            Request.refreshToken();
            Boards.saving = false;
            Modal.close(Modal.modals.edit);
        });
    },
    removeTask: function() {
        Request.removeTask(Modal.taskToRemove).then(function(response) {
            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);
            });

            if (response.status !== 'success') {
                return;
            }

            var boardData = response.data[1][0];

            Actions.updateActiveBoard(boardData);
            Request.refreshToken();
            Modal.close(Modal.modals.view);
        });
    },
    updateTask: function() {
        Boards.saving = true;

        if (!Task.validateTask(Modal.modalProps)) {
            Boards.saving = false;
            return;
        }

        Request.updateTask(Modal.modalProps).then(function(response) {
            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);
            });

            if (response.status !== 'success') {
                return;
            }

            var boardData = response.data[2][0];

            Actions.updateActiveBoard(boardData);
            Modal.close(Modal.modals.edit);
            Request.refreshToken();
            Boards.saving = false;
        });
    },
    moveColumn: function(e, task) {
        if (e.target.tagName !== 'SELECT') {
            return;
        }
        task.column_id = parseInt(e.target.value);

        Request.updateTask(task).then(function(response) {
            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);
            });
            if (response.status === 'success') {
                console.info('success', this);
                var boardData = response.data[2][0];

                Actions.updateActiveBoard(boardData);
            }
        });
    },
    replaceUpdatedTask: function(updatedTask) {
        Boards.activeboard.columns.forEach(function(column) {
            if (parseInt(column.data.id) !== parseInt(updatedTask.column_id)) {
                return;
            }

            column.data.tasks.forEach(function(task) {
                if (parseInt(task.id) !== parseInt(updatedTask.id)) {
                    return;
                }

                Actions.updateTaskComments(task, updatedTask.ownComment);
            });
        });
    },
    updateTaskComments: function(task, newComments) {
        task.comments = [];

        newComments.forEach(function(comment) {
            task.comments.push(Comment.fromData(comment));
        });
        Modal.viewModalProps = Actions.taskFromData(task);
    },
    copyTaskToBoard: function(e, task) {
        if (e.target.tagName !== 'SELECT') {
            return;
        }

        var boardid = e.target.value;
        var boardData = {};

        var boardData = {};
        Boards.boards.forEach(function(board) {
            if (board.id === boardid) {
                task.column_id = board.columns[0].data.id;
                boardData = board;
            }
        });

        Request.addTask(task).then(function(response) {
            if (response.status === 'success') {
                Notifications.addNote('success', Strings.json.boards_task +
                    ' ' + task.title +
                    ' ' + Strings.json.boards_taskCopied +
                    ' ' + boardData.name);

                Actions.getBoards();

                return;
            }

            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);
            });
        });
    },
    moveTaskToBoard: function(e, task) {
        if (e.target.tagName !== 'SELECT') {
            return;
        }

        var boardid = e.target.value;
        var boardData = {};

        Boards.boards.forEach(function(board) {
            if (board.id === boardid) {
                task.column_id = board.columns[0].data.id;
                boardData = board;
            }
        });

        Request.updateTask(task).then(function(response) {
            if (response.status === 'success') {
                Notifications.addNote('success', Strings.json.boards_task + ' ' +
                    task.title + ' ' + Strings.json.boards_taskMoved + ' ' +
                    boardData.name);

                Actions.getBoards();

                return;
            }

            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);
            });
        });
    },
    saveLimitChanges: function(data) {
        var originalLimit = data.data.task_limit;

        data.data.task_limit = parseInt(data.newlimit);

        Request.updateColumn(data.data).then(function(response) {
            response.alerts.forEach(function(note) {
                Notifications.addNote(note.type, note.text);
            });

            if (response.status !== 'success') {
                data.data.task_limit = originalLimit;
                return;
            }

            var colData = response.data[1][0];
            data = Actions.columnFromData(colData, data);
        });

        data.showLimitEditor = false;
    },
    quickAddClicked: function(data) {
        if (data.quickAdd.title === '') {
            Actions.showModal(data);
            return;
        }

        data.quickAdd.column_id = data.data.id;
        Actions.addTask(data.quickAdd);

        data.quickAdd = Task.empty();
    }
};

module.exports = Actions;
