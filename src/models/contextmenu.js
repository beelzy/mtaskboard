var m = require('mithril');

var ContextMenu = {
    init: function() {
        document.addEventListener('click', ContextMenu.closeAllMenus);
    },
    isOpen: false,
    menu: {
        x: 0,
        y: 0
    },
    model: {},
    open: function(e, model) {
        e.preventDefault();
        e.stopPropagation();
        ContextMenu.closeAllMenus();

        window.addEventListener('scroll', ContextMenu.closeAllMenus, true);
        ContextMenu.isOpen = true;
        ContextMenu.model = model;

        var edgeBuffer = 10;

        ContextMenu.menu.x = e.pageX + 'px';
        ContextMenu.menu.y = e.pageY + 'px';

        // Can we do better?
        var adjustPosition = function() {
            var target = document.querySelector('.context-menu-container.menu-open');
            var rect = target.getBoundingClientRect();

            var offsetX = (e.pageX + rect.width + edgeBuffer) > window.innerWidth;
            var offsetY = (e.pageY + rect.height + edgeBuffer) > window.innerHeight;

            ContextMenu.menu.x = e.pageX - (offsetX ? rect.width : 0) + 'px';
            ContextMenu.menu.y = e.pageY - (offsetY ? rect.height : 0) + 'px';

            m.redraw();
        };

        setTimeout(adjustPosition, 0);
    },
    closeAllMenus: function() {
        window.removeEventListener('scroll', ContextMenu.closeAllMenus, true);
        ContextMenu.isOpen = false;

        m.redraw();
    }
};

module.exports = ContextMenu;
