var Strings = require('./strings.js');
var AutoAction = require('./autoaction.js');

var AutoActions = {
    loading: true,
    noActionsMessage: '',
    hasInactiveBoards: false,
    newAction: AutoAction.empty(),
    triggerSources: [],
    actionSources: [],
    isAddDisabled: true,
    actionToRemove: null,
    triggers: function() {
        return [
            [
                1,
                Strings.json.settings_triggerMoveToColumn,
            ], [
                2,
                Strings.json.settings_triggerAssignedToUser,
            ], [
                3,
                Strings.json.settings_triggerAddedToCategory,
            ], [
                4,
                Strings.json.settings_triggerPointsChanged
            ]
        ];
    },
    types: function() {
        if (AutoActions.newAction.trigger === 4) {
            return [
                [
                    7,
                    Strings.json.settings_alterByPoints
                ]
            ];
        } 
        return AutoActions.typesList();
    },
    typesList: function() {
        return [
            [1,
                Strings.json.settings_actionSetColor,
            ], [
                2,
                Strings.json.settings_actionSetCategory,
            ], [
                3,
                Strings.json.settings_actionAddCategory,
            ], [
                4,
                Strings.json.settings_actionSetAssignee,
            ], [
                5,
                Strings.json.settings_actionAddAssignee,
            ], [
                6,
                Strings.json.settings_actionClearDueDate
            ]
        ]
    },
    triggerSources: [],
    actionSources: [],
    checkAddDisabled: function() {
        AutoActions.isAddDisabled = false;

        if (AutoActions.newAction.board_id === null) {
            AutoActions.isAddDisabled = true;
            return;
        }

        if (AutoActions.newAction.source_id === null) {
            AutoActions.isAddDisabled = AutoActions.newAction.trigger !== 4;
        }

        if (!AutoActions.isAddDisabled && AutoActions.newAction.change_to === null) {
            AutoActions.isAddDisabled = AutoActions.newAction.type !== 6;
        }
    }
};

module.exports = AutoActions;
