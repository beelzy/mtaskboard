var m = require('mithril');

var Request = require('../utils/request.js');
var User = require('./user.js');

var Column = {
    ownTask: [],
    hasTaskLimit: function(column) {
        return column.data.task_limit > 0;
    },
    sortTasks: function(data) {
        switch(data.sortOption) {
            case 'pos':
                data.data.tasks.sort(function(a, b) {
                    return a.position - b.position;
                });
                break;
            case 'due':
                data.data.tasks.sort(function(a, b) {
                    return new Date(a.due_date).getTime() - new Date(b.due_date).getTime();
                });
                break;
            case 'pnt':
                data.data.tasks.sort(function(a, b) {
                    return b.points - a.points;
                });
                break;
        }
    },
    toggleTaskCollapse: function(data) {
        data.collapseTasks = !data.collapseTasks;
        data.data.tasks.forEach(function(task) {
            task.isCollapsed = data.collapseTasks;
        });
    },
    toggleCollapsed: function(data) {
        data.isCollapsed = !data.isCollapsed;

        Request.toggleCollapsed(User.user.id, data.data.id).then(function(response) {
            User.user.collapsed = response[1];
        });
    },
    beginLimitEdit: function(data) {
        data.showLimitEditor = true;
    },
    cancelLimitChanges: function(data) {
        data.showLimitEditor = false;
    }
};

module.exports = Column;
