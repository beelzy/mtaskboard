var m = require('mithril');
var marked = require('marked');
var Request = require('../utils/request.js');
var Notifications = require('./notifications.js');
var Modal = require('./modal.js');
var Page = require('./page.js');
var DND = require('mithril-dnd');

var Boards = {
    boardNavId: null,
    boards: [],
    activeboard: {},
    showBoard: false,
    loading: false,
    hideFiltered: false,
    userFilter: null,
    categoryFilter: null,
    saving: false,
    empty: function(title) {
        return {
            id: 0,
            title: title,
            name: '',
            is_active: true,
            columns: [],
            categories: [],
            issue_trackers: [],
            users: [],
            categoryDefaultColor: '#ffffe0',
            newColumnName: '',
            newCategoryName: '',
            issueTrackerUrl: '',
            issueTrackerBugId: ''
        };
    },
    toData: function(board) {
        var data = {};
        data.id = board.id;
        data.name = board.name;
        data.is_active = board.is_active;

        data.columns = [];
        board.columns.forEach(function(column) {
            data.columns.push(column.data);
        });

        data.categories = board.categories;
        data.auto_actions = board.auto_actions;
        data.issue_trackers = board.issue_trackers;
        data.users = board.users;
        return data;
    },
    drop: function(data) {
        var id = data.data.target.id;
        var targetcolumn = data.data.to.columnIndex;
        var fromcolumn = data.data.from.columnIndex;

        if (fromcolumn !== targetcolumn) {
            Boards.changeTaskColumn(id, targetcolumn);
            return;
        }

        for (var i = 0, size = Boards.activeboard.columns.length; i < size; ++i) {
            var column = Boards.activeboard.columns[i];

            if (parseInt(column.data.id) === parseInt(targetcolumn)) {
                var pos = 0;
                Boards.activeboard.columns[i].data.tasks.forEach(function(task) {
                    task.position = pos;
                    pos++;
                });
                Request.updateColumn(column.data);
                break;
            }
        }
    },
    clearBoards: function() {
        Boards.boardNavId = null;
        Boards.activeboard = {};
        Boards.boards = [];
        DND.destroy();
    },
    setActiveBoard: function() {
        if (!Boards.boardNavId || !Boards.boards) {
            Boards.activeboard = null;
            return;
        }

        Boards.boards.forEach(function(board) {
            if (board.id === Boards.boardNavId) {
                Boards.activeboard = board;
                Page.pagename = board.name;
            }
        });
    },
    goToBoard: function(id) {
        if (id === null) {
            return;
        }
        m.route.set('/boards/' + id);
    },
    sortBoard: function(board) {
        board.columns.sort(function(a, b) {
            return parseInt(a.data.position) - parseInt(b.data.position);
        })
    },
    sortBoards: function() {
        Boards.boards.forEach(function(board) {
            Boards.sortBoard(board);
        });
    },
    noBoards: function() {
        if (!Boards.boards || Boards.boards.length === 0) {
            return true;
        }

        return false;
    },
    toggleFiltered: function() {
        Boards.hideFiltered = !Boards.hideFiltered;
        Boards.activeboard.columns.forEach(function(column) {
            column.data.tasks.forEach(function(task) {
                task.hideFiltered = Boards.hideFiltered;
            });
        });
    },
    filterTasks: function() {
        Boards.activeboard.columns.forEach(function(column) {
            column.data.tasks.forEach(function(task) {
                task.filtered = false;

                if (Boards.userFilter) {
                    var found = false;

                    if (Boards.userFilter === -1 && task.assignees.length === 0) {
                        found = true;
                    }

                    task.assignees.forEach(function(user) {
                        if (user.id === Boards.userFilter) {
                            found = true;
                        }
                    });

                    if (!found) {
                        task.filtered = true;
                    }
                }

                if (Boards.categoryFilter) {
                    var found = false;

                    if (Boards.categoryFilter === -1 && task.categories.length === 0) {
                        found = true;
                    }

                    task.categories.forEach(function(cat) {
                        if (cat.id === Boards.categoryFilter) {
                            found = true;
                        }
                    });

                    if (!found) {
                        task.filtered = true;
                    }
                }
            });
        });
    },
    updateTaskColorByCategory: function(e) {
        var categories = {};
        Boards.activeboard.categories.forEach(function(category) {
            categories[category.id] = category;
        });
        var selectedOptions = Array.from(e.target.selectedOptions);
        Modal.modalProps.categories = [];
        selectedOptions.map(function(option) {
            Modal.modalProps.categories.push(categories[option.value]);
        });
        Modal.modalProps.color = selectedOptions[selectedOptions.length - 1].getAttribute('color');
    },
    updateTaskAssignees: function(e) {
        var assignees = {};
        Boards.activeboard.users.forEach(function(assignee) {
            assignees[assignee.id] = assignee;
        });
        var selectedOptions = Array.from(e.target.selectedOptions);
        Modal.modalProps.assignees = [];
        selectedOptions.map(function(option) {
            Modal.modalProps.assignees.push(assignees[option.value]);
        });
    },
    getUserName: function (userid) {
        var user = Boards.activeboard.users.filter(function(test) {
            return parseInt(test.id) === parseInt(userid);
        })[0];

        return user.username;
    },
    confirmRemoveTask: function(taskid) {
        Modal.taskToRemove = taskid;
        Modal.open(Modal.modals.removetask);
    },
    beginEditComment: function(comment) {
        Modal.commentEdit = { ...comment };
    },
    confirmRemoveComment: function(comment) {
        Modal.commentToRemove = comment;
        Modal.open(Modal.modals.removecomment);
    },
    markedCallback: function(error, text) {
        Boards.activeboard.issue_trackers.forEach(function(tracker) {
            var re = new RegExp(tracker.regex, 'ig');
            var replacements = [];
            var result = re.exec(text);

            while (result !== null) {
                var link = '<a href="' + tracker.url.replace(/%BUGID%/g, result[1]) +
                    '" target="tb_external" rel="noreferrer">' +
                    result[0] + '</a>';

                replacements.push({
                    str: result[0],
                    link
                });
                result = re.exec(text);
            }

            for (var i = replacements.length - 1; i >= 0; --i) {
                text = text.replace(replacements[i].str,
                    replacements[i].link);
            }
        });

        return text;
    },
    getTaskDescription: function(data) {
        var html = marked(data.description, Boards.markedCallback);

        html = html.replace(/(\{)([^}]+)(\})/g, '{{ "{" }}$2{{ "}" }}');

        return html + ' ';
    },
    changeTaskColumn: function(id, to) {
        var column = Boards.activeboard.columns[to];

        for(var j = 0, tsize = column.data.tasks.length; j < tsize; j++) {
            var task = column.data.tasks[j];
            if (parseInt(id) === parseInt(task.id)) {
                task.column_id = parseInt(column.data.id);
                Request.updateTask(task);
                break;
            }
        }
    }
};

module.exports = Boards;
