var Request = require('../utils/request.js');
var User = require('./user.js');
var Modal = require('./modal.js');
var Notifications = require('./notifications.js');
var Strings = require('./strings.js');

var UserAdmin = {
    loading: true,
    userToRemove: {},
    showModal: function(isAdd, user) {
        user = user || User.empty();
        var boardAccess = [];
        user.board_access.forEach(function(id) {
            boardAccess.push('' + id);
        });
        Modal.modalProps = {
            prefix: isAdd,
            user: {...user,
                password: '',
                password_verify: '',
                boardAccess: boardAccess
            }
        };

        Modal.open(Modal.modals.edituser);
    },
    validateModalUser: function() {
        var user = Modal.modalProps.user;

        if (user.username === '') {
            Notifications.addNote('error', Strings.json.settings_usernameRequired);
            return false;
        }

        if (Modal.modalProps.prefix && user.password === '') {
            Notifications.addNote('error', Strings.json.settings_passwordRequired);

            return false;
        }

        if (user.password !== user.password_verify) {
            Notifications.addNote('error', Strings.json.settings_verifyError);

            return false;
        }

        var emailRegex = /.+@.+\..+/i;
        var match = user.email.match(emailRegex);

        if (!match && user.email !== '') {
            Notifications.addNote('error', Strings.json.settings_emailError);

            return false;
        }

        return true;
    },
    showConfirmModal: function(userid) {
        UserAdmin.userToRemove = userid;

        Modal.open(Modal.modals.removeuser);
    }
};

module.exports = UserAdmin;
