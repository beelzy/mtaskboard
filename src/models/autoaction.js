var AutoAction = {
    empty: function() {
        return {
            id: 0,
            trigger: 1,
            source_id: null,
            type: 1,
            change_to: null,
            board_id: null
        };
    },
    fromData: function(action) {
        var newAction = {};
        newAction.id = parseInt(action.id);
        newAction.trigger = parseInt(action.trigger);
        newAction.source_id = parseInt(action.source_id);
        newAction.type = parseInt(action.type);
        newAction.change_to = action.change_to;
        newAction.board_id = parseInt(action.board_id);

        return newAction;
    }
};

module.exports = AutoAction;
