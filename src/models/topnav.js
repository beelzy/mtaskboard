var m = require('mithril');
var Notifications = require('./notifications.js');
var Request = require('../utils/request.js');
var Boards = require('./boards.js');

var TopNav = {
    logout: function() {
        Request.logout().then(function(response) {
            var alert = response.alerts[0];
            Boards.clearBoards();

            Notifications.addNote(alert.type, alert.text);
            m.route.set('/');
        });
    }
};

module.exports = TopNav;
