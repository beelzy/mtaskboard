var Category = {
    fromData: function(category) {
        var newCategory = {};
        newCategory.id = parseInt(category.id);
        newCategory.name = category.name;
        newCategory.default_task_color = category.default_task_color;
        newCategory.board_id = parseInt(category.board_id);
        return newCategory;
    }
};

module.exports = Category;
