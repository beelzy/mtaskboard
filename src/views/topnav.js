var m = require('mithril');
var TopNav = require('../models/topnav.js');
var Page = require('../models/page.js');
var User = require('../models/user.js');
var Strings = require('../models/strings.js');

module.exports = {
    onupdate: function(vnode) {
        document.title = 'mTaskboard - ' + Strings.json.settings;
    },
    view: function(vnode) {
        var fallbackPagename = vnode.attrs.pagename + ' ';
        return m('nav.nav-top', [
            m('h1', [
                'mTaskBoard', 
                m('span', m('small', [
                    ' - ' + (Page.pagename ? Page.pagename + ' ' : fallbackPagename),
                    m('small.dark.small', 'v' + Page.version)
                ]))
            ]),
            m('.buttons', [
                m('button' + (m.route.get().startsWith('/dashboard') ? '' : '.flat'), {onclick: function() {
                    m.route.set('/dashboard');
                }}, Strings.json.dashboard),
                ' ',
                m('button' + (m.route.get().startsWith('/boards') ? '' : '.flat'), {onclick: function() {
                    m.route.set('/boards');
                }}, Strings.json.boards),
                ' ',
                m('button' + (m.route.get().startsWith('/settings') ? '' : '.flat'), {onclick: function() {
                    m.route.set('/settings');
                }}, Strings.json.settings),
                ' ',
                m('button.flat',
                    {onclick: TopNav.logout},
                    Strings.json.logout + ' (' + User.user.username + ')'
                )
            ])
        ]);
    }
};
