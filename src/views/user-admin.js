var m = require('mithril');

var UserAdmin = require('../models/user-admin.js');
var User = require('../models/user.js');
var Modal = require('./modal/modal.js');
var ModalModel = require('../models/modal.js');
var RemoveUser = require('./modal/removeuser.js');
var EditUser = require('./modal/edituser.js');
var Strings = require('../models/strings.js');

module.exports = {
    view: function(vnode) {
        return [
            m('section', [
                m('h2', Strings.json.settings_userAdmin),
                ' ',
                m('.row', [
                    m('table.alternating', [
                        m('thead', m('tr', [
                            m('th', Strings.settings_user),
                            m('th', Strings.settings_email),
                            m('th', Strings.settings_securityLevel),
                            m('th', Strings.settings_defaultBoard),
                            m('th', Strings.settings_actions)
                        ])),
                        UserAdmin.loading ? '' : m('tbody',
                            User.users.map(function(user) {
                                return m('tr', [
                                    m('td', user.username),
                                    m('td', user.email),
                                    m('td', user.security_level_name),
                                    m('td', user.default_board_name),
                                    m('td', user.can_admin ? m('span', [
                                        m('a.padded', {onclick: function(e) {
                                            e.preventDefault();
                                            UserAdmin.showModal(false, user);
                                        }, href: '', title: Strings.json.settings_editUser}, m('i.icon.icon-edit.color-primary')),
                                        m('a.padded', {onclick: function(e) {
                                            e.preventDefault();
                                            UserAdmin.showConfirmModal(user.id);
                                        }, href: '', title: Strings.settings_removeUser}, m('i.icon.icon-trash-empty.color-secondary'))
                                    ]) : '')
                                ]);
                            })
                            )
                    ]),
                    ' ',
                    UserAdmin.loading ? m('.center', m('.spinner')) : '',
                    ' ',
                    User.user && User.user.security_level < 3 ? m('button', {onclick: UserAdmin.showModal}, [m('i.icon.icon-plus'), ' ', Strings.json.settings_addUser]) : ''
                ])
            ]),
            m('tb-modal', m(Modal, {'modal-title': 'Confirm User Removal', blocking: true, 'modal-id': 'user-remove-confirm', contents: RemoveUser, modal: ModalModel.modals.removeuser})),
            ModalModel.modals.edituser.isOpen && ModalModel.modalProps.hasOwnProperty('user') ? m('tb-modal', m(Modal, {'modal-id': 'user-addEdit-form', contents: EditUser, modal: ModalModel.modals.edituser})) : ''
        ];
    }
};
