var m = require('mithril');
var TopNav = require('./topnav.js');
var Settings = require('../models/settings.js');
var UserSettings = require('./user-settings.js');
var UserAdmin = require('./user-admin.js');
var BoardAdmin = require('./board-admin.js');
var AutoActions = require('./autoactions.js');
var Strings = require('../models/strings.js');
var DND = require('mithril-dnd');

module.exports = {
    view: function(vnode) {
        return [
            m('tb-top-nav', m(TopNav, {pagename: Strings.json.settings})),
            m('.settings', [
                m('.half-page', [
                    m('tb-user-settings', m(UserSettings)),
                    m('tb-user-admin', m(UserAdmin))
                ]),
                m('.half-page', [
                    m('tb-board-admin', m(BoardAdmin)),
                    m('tb-auto-actions', m(AutoActions))
                ])
            ]),
            m(DND.mirror, {selector: '.modal-list'})
        ];
    }
};
