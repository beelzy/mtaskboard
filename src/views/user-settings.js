var m = require('mithril');

var UserSettings = require('../models/user-settings.js');
var User = require('../models/user.js');
var Settings = require('../models/settings.js');
var Strings = require('../models/strings.js');

module.exports = {
    view: function(vnode) {
        return m('section', [
            m('h2', Strings.json.settings_userSettings),
            m('.half', [
                m('h3', Strings.json.settings_changePassword),
                m('form', [
                    m('label.hidden', {for: 'currentPassword'}, Strings.json.settings_currentPassword),
                    m('input#currentPassword', {value: UserSettings.changePassword.current, onchange: function(e) {
                        UserSettings.changePassword.current = e.target.value;
                    }, type: 'password', name: 'currentPassword', placeholder: Strings.json.settings_currentPassword}),
                    ' ',
                    m('label.hidden', {for: 'newPassword'}, Strings.json.settings_newPassword),
                    m('input#newPassword', {value: UserSettings.changePassword.newPass, onchange: function(e) {
                        UserSettings.changePassword.newPass = e.target.value;
                    }, type: 'password', name: 'newPassword', placeholder: Strings.json.settings_newPassword}),
                    ' ',
                    m('label.hidden', {for: 'verifyPassword'}, Strings.json.settings_verifyPassword),
                    m('input#verifyPassword', {value: UserSettings.changePassword.verPass, onchange: function(e) {
                        UserSettings.changePassword.verPass = e.target.value;
                    }, type: 'password', name: 'verifyPassword', placeholder: Strings.json.settings_verifyPassword}),
                    ' ',
                    m('button', {disabled: UserSettings.changePassword.submitted, onclick: function(e) {
                        e.preventDefault();
                        Settings.changePassword();
                    }, type: 'submit'}, Strings.json.settings_changePassword),
                    ' ',
                    m('button.flat', {onclick: function(e) {
                        e.preventDefault();
                        UserSettings.resetPasswordForm();
                    }, disabled: UserSettings.changePassword.submitted}, Strings.json.reset)
                ]),
                ' ',
                m('h3.tall', Strings.json.settings_globalOptions),
                m('label', [
                    Strings.json.settings_displayLanguage,
                    ' ',
                    User.options && Object.keys(User.options).length > 0 ? m('select.autosize', {onchange: function(e) {
                        User.options.language = e.target.value;
                        Settings.updateUserOptions();
                    }}, [
                        m('option', {value: 'en', selected: User.options.language === 'en'}, 'English'),
                        m('option', {value: 'de', selected: User.options.language === 'de'}, 'Deutsch'),
                        m('option', {value: 'es', selected: User.options.language === 'es'}, 'Español')
                    ]) : '',
                    ' ',
                    m('a', {href:'https://github.com/kiswa/TaskBoard/wiki/Translations', target: 'translations', rel: 'noopener noreferrer'}, 'Add Yours?')
                ]),
                m('label', [
                    Strings.json.settings_selectDefaultBoard,
                    ' ',
                    User.user ? m('select.autosize', {onchange: function(e) {
                        User.user.default_board_id = parseInt(e.target.value, 10);
                        Settings.changeDefaultBoard();
                    }}, [
                        m('option', {value: 0}, Strings.json.none),
                        Settings.boards.map(function(board) {
                            return m('option', {value: board.id, selected: parseInt(User.user.default_board_id) === parseInt(board.id)}, board.name);
                        })
                    ]) : ''
                ]),
                m('label', [
                    Strings.json.settings_newTasks,
                    ' ',
                    User.options && Object.keys(User.options).length ? m('select.autosize', {onchange: function(e) {
                        User.options.new_tasks_at_bottom = parseInt(e.target.value);
                        Settings.updateUserOptions();
                    }}, [
                        m('option', {value: 0, selected: parseInt(User.options.new_tasks_at_bottom) === 0}, Strings.json.settings_bottom),
                        m('option', {value: 1, selected: parseInt(User.options.new_tasks_at_bottom) === 1}, Strings.json.settings_top)
                    ]) : ''
                ])
            ]),
            m('.half', [
                m('h3', Strings.json.settings_changeUsername),
                m('form', [
                    m('label.hidden', {for: 'username'}, Strings.settings_newUsername),
                    m('input#username', {value: UserSettings.changeUsername.newName, onchange: function(e) {
                        UserSettings.changeUsername.newName = e.target.value;
                    }, type: 'text', name: 'username', placeholder: Strings.json.settings_newUsername}),
                    ' ',
                    m('button', {disabled: UserSettings.changeUsername.submitted, onclick: function(e) {
                        e.preventDefault();
                        Settings.updateUsername();
                    }, type: 'submit'}, Strings.json.settings_changeUsername),
                    ' ',
                    m('button.flat', {disabled: UserSettings.changeUsername.submitted, onclick: function(e) {
                        e.preventDefault();
                        UserSettings.resetUsernameForm();
                    }}, Strings.json.reset)
                ]),
                ' ',
                m('form', [
                    m('h3.tall', Strings.json.settings_changeEmail),
                    m('label.hidden', {for: 'email'}, Strings.json.settings_newEmail),
                    m('input#email', {value: UserSettings.changeEmail.newEmail, onchange: function(e) {
                        UserSettings.changeEmail.newEmail = e.target.value;
                    }, type: 'text', name: 'email', placeholder: Strings.json.settings_newEmail + ' - ' + Strings.json.settings_blank}),
                    ' ',
                    m('button', {disabled: UserSettings.changeEmail.submitted, onclick: function(e) {
                        e.preventDefault();
                        Settings.updateEmail();
                    }, type: 'submit'}, Strings.json.settings_changeEmail),
                    ' ',
                    m('button.flat', {disabled: UserSettings.changeEmail.submitted, onclick: function(e) {
                        e.preventDefault();
                        UserSettings.resetEmailForm();
                    }}, Strings.json.reset)
                ]),
                ' ',
                User.options && Object.keys(User.options).length > 0 ? m('.hold-bottom', [
                    m('label', [
                        Strings.json.settings_optionsDisplay,
                        ' ',
                        m('input.hidden', {type: 'checkbox', checked: parseInt(User.options.multiple_tasks_per_row), onclick: function(e) {
                            User.options.multiple_tasks_per_row = e.target.checked ? 1 : 0;
                            Settings.updateUserOptions();
                        }}),
                        m('span.toggle')
                    ]),
                    m('label', [
                        Strings.json.settings_optionsAnimate,
                        ' ',
                        m('input.hidden', {type: 'checkbox', checked: parseInt(User.options.show_animations), onclick: function(e) {
                            User.options.show_animations = e.target.checked ? 1 : 0;
                            Settings.updateUserOptions();
                        }}),
                        m('span.toggle')
                    ]),
                    m('label', [
                        Strings.json.settings_optionsAssignee,
                        ' ',
                        m('input.hidden', {type: 'checkbox', checked: parseInt(User.options.show_assignee), onclick: function(e) {
                            User.options.show_assignee = e.target.checked ? 1 : 0;
                            Settings.updateUserOptions();
                        }}),
                        m('span.toggle')
                    ])
                ]) : ''
            ])
        ]);
    }
};
