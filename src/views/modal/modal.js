var m = require('mithril');

var Modal = require('../../models/modal.js');
var User = require('../../models/user.js');

module.exports = {
    view: function(vnode) {
        var data = vnode.attrs.data;
        var modal = vnode.attrs.modal;
        var animated = User.options && Object.keys(User.options).length > 0 ? parseInt(User.options.show_animations) : true;
        var modalTitle = vnode.attrs['modal-title'] ? vnode.attrs['modal-title'] : '';
        var contents = vnode.attrs.contents ? vnode.attrs.contents : '';
        return m('.modal-container' + (modal.isOpen ? '.modal-open' : '')
            + (animated ? '.animated' : ''), {onclick: function(e) {
                if (e.currentTarget === e.target) {
                    Modal.close(modal, true);
                }
            }, oncontextmenu: function(e) {
                e.preventDefault();
                e.stopPropagation();
            }},
            m('.modal' + (modal.isOpen ? '' : '.modal-closed')
                + (animated ? '.animated' : '')
                + (modal.wide ? '.wide' : ''), [
                    modalTitle ? m('.title', m('h2', [
                        modalTitle,
                        m('span.right', m('i.icon.icon-cancel', {onclick: function() {
                            Modal.close(modal);
                        }}))
                    ])) : '',
                    m('.body', m(contents, {data: data}))
                ]));
    }
};
