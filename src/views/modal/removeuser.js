var m = require('mithril');

var Modal = require('../../models/modal.js');
var Settings = require('../../models/settings.js');
var Strings = require('../../models/strings.js');

module.exports = {
    view: function(vnode) {
        return [
            m('.center', [
                Strings.json.settings_removeUserWarning,
                m('br'),
                Strings.json.settings_continue
            ]),
            ' ',
            m('.buttons', [
                m('button.flat', {onclick: Settings.removeUser}, Strings.json.yes),
                ' ',
                m('button', {onclick: function() {Modal.close(Modal.modals.removeuser);}}, Strings.json.no)
            ])
        ];
    }
};
