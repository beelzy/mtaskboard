var m = require('mithril');

var Settings = require('../../models/settings.js');
var Modal = require('../../models/modal.js');
var BoardAdmin = require('../../models/board-admin.js');
var Strings = require('../../models/strings.js');

module.exports = {
    view: function(vnode) {
        return [
            m('.center', [
                Strings.json.settings_removeBoardWarning,
                m('br'),
                Strings.json.settings_continue
            ]),
            ' ',
            m('.buttons', [
                m('button.flat', {disabled: Settings.saving, onclick: Settings.removeBoard}, Strings.json.yes),
                ' ',
                m('button', {disabled: Settings.saving, onclick: function() {
                    Modal.close(Modal.modals.removeboard);
                }}, Strings.json.no)
            ])
        ];
    }
};
