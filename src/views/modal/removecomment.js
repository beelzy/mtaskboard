var m = require('mithril');

var Modal = require('../../models/modal.js');
var Actions = require('../../models/actions.js');
var Strings = require('../../models/strings.js');

module.exports = {
    view: function(vnode) {
        return [
            m('.center', [
                Strings.json.boards_confirmWarningComment,
                m('br'),
                Strings.json.boards_confirmContinue
            ]),
            m('.buttons', [
                m('button.flat', {onclick: function() {
                    Modal.close(Modal.modals.removecomment);
                    Actions.removeComment();
                }}, Strings.json.yes),
                ' ',
                m('button', {onclick: function() {
                    Modal.close(Modal.modals.removecomment);
                }}, Strings.json.no)
            ])
        ];
    }
};
