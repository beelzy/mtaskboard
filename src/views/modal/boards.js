var m = require('mithril');

var Boards = require('../../models/boards.js');
var Column = require('../../models/column.js');
var Modal = require('./modal.js');
var ModalModel = require('../../models/modal.js');
var Strings = require('../../models/strings.js');
var View = require('./view.js');
var AddTask = require('./addtask.js');
var RemoveTask = require('./removetask.js');
var RemoveComment = require('./removecomment.js');

module.exports = {
    view: function(vnode) {
        var hasViewProps = Object.keys(ModalModel.viewModalProps).length > 0;
        var hasProps = Object.keys(ModalModel.modalProps).length > 0;
        return [
            Boards.activeboard && hasViewProps ?
            m('tb-modal.view-modal',
                m(Modal, {
                    data: ModalModel.data,
                    'modal-title': Strings.json.boards_viewTask + ' - ' + ModalModel.viewModalProps.title,
                    'modal-id': ModalModel.MODAL_VIEW_ID + ModalModel.data.data.id,
                    contents: View, modal: ModalModel.modals.view
                })) : '',
            Boards.activeboard && hasProps ?
            m('tb-modal',
                m(Modal, {
                    'modal-title': (ModalModel.modalProps.id ?
                        Strings.json.boards_editTask :
                        Strings.json.boards_addTask),
                    'modal-id': ModalModel.MODAL_ID + ModalModel.data.data.id,
                    contents: AddTask, modal: ModalModel.modals.edit
                })
            ) : '',
            ModalModel.data.data ?
            m('tb-modal',
                m(Modal, {
                    'modal-title': Strings.json.boards_confirmRemoveTask,
                    'modal-id': ModalModel.MODAL_CONFIRM_ID + ModalModel.data.data.id,
                    contents: RemoveTask, modal: ModalModel.modals.removetask
                })
            ) : '',
            ModalModel.data.data ?
            m('tb-modal',
                m(Modal, {
                    'modal-title': Strings.json.boards_confirmRemoveComment,
                    'modal-id': ModalModel.MODAL_CONFIRM_COMMENT_ID + ModalModel.data.data.id,
                    contents: RemoveComment,
                    modal: ModalModel.modals.removecomment
                })
            ) : ''
        ];
    }
};
