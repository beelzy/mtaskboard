var m = require('mithril');

var Strings = require('../../models/strings.js');
var Modal = require('../../models/modal.js');
var Settings = require('../../models/settings.js');

module.exports = {
    view: function(vnode) {
        return [
            m('.center', [
                'Removing an automatic action cannot be undone.',
                m('br'),
                'Continue?'
            ]),
            ' ',
            m('.buttons', [
                m('button.flat', {onclick: Settings.removeAction}, Strings.json.yes),
                ' ',
                m('button', {onclick: function() {Modal.close(Modal.modals.removeautoaction);}}, Strings.json.no)
            ])
        ];
    }
};
