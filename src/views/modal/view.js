var m = require('mithril');
var marked = require('marked');

var Modal = require('../../models/modal.js');
var Actions = require('../../models/actions.js');
var Boards = require('../../models/boards.js');
var Task = require('../../models/task.js');
var User = require('../../models/user.js');
var Comment = require('../../models/comment.js');
var Strings = require('../../models/strings.js');

module.exports = {
    view: function(vnode) {
        var data = vnode.attrs.data;
        Task.checkDueDate(Modal.viewModalProps);
        return [
            Modal.viewModalProps.points ? m('div', m('span.badge', Modal.viewModalProps.points)) : '',
            m('.details', {
                style: 'background-color: '
                + Modal.viewModalProps.color
                + '; color: '
                + Task.getTextColor(Modal.viewModalProps.color)
            }, [
                Modal.viewModalProps.due_date ?
                m('.date'
                    + (Modal.viewModalProps.isOverDue ? '.isOverDue' : '')
                    + (Modal.viewModalProps.isNearlyDue ? '.isNearlyDue' : ''),
                    Strings.json.boards_taskDue + ' ' + Modal.viewModalProps.due_date
                ) : '',
                Modal.viewModalProps.description.length ?
                m('.description', m.trust(marked(Modal.viewModalProps.description))) : '',
                m('.stats', [
                    m('div', [
                        m('strong', Strings.json.boards_taskAssignees),
                        Modal.viewModalProps.assignees.map(function(assignee) {
                            return m('span', assignee.username);
                        }),
                        Modal.viewModalProps.assignees.length === 0 ?
                        m('span', Strings.json.boards_filterByUnassigned) : '',
                    ]),
                    m('div', [
                        m('strong', Strings.json.boards_taskCategories),
                        Modal.viewModalProps.categories.map(function(category) {
                            return m('span', category.name);
                        }),
                        Modal.viewModalProps.categories.length === 0 ?
                        m('span', Strings.json.boards_filterByUncategorized) : ''
                    ]),
                    m('div', [m('strong', Strings.json.boards_taskCurrentColumn), data.data.name])
                ])
            ]),
            m('.quick-actions', [
                m('button.flat', {onclick: function() {
                    Modal.close(Modal.modals.view);
                    Actions.showModal(data, Modal.viewModalProps);
                }}, Strings.json.boards_editTask),
                m('button.flat', {onclick: function() {
                    Boards.confirmRemoveTask(Modal.viewModalProps.id);
                }}, Strings.json.boards_removeTask)
            ]),
            Modal.viewModalProps.attachments.length ? m('div',
                m('h3', Strings.json.boards_taskAttachments)
            ) : '',
            m('div', [
                m('h3', Strings.json.boards_taskAddAttachment),
                m('input', {type: 'file'}),
                ' ',
                m('button', [m('i.icon.icon-upload'), ' ', Strings.json.boards_taskUpload])
            ]),
            Modal.viewModalProps.comments.length ? m('div', [
                m('h3', Strings.json.boards_taskComments),
                m('.comment', Modal.viewModalProps.comments.map(function(comment) {
                    return [
                        !comment.isEdit ? m('div', m.trust(marked(comment.text))) : '',
                        comment.isEdit ? m('textarea', {onchange: function(e) {
                            Modal.commentEdit.text = e.target.value;
                        }, value: Modal.commentEdit.text}) : '',
                        m('.byline', [
                            comment.is_edited ? Strings.json.boards_editedBy : Strings.json.boards_postedBy,
                            Boards.getUserName(comment.user_id) + '—',
                            new Date(comment.timestamp).toLocaleString()
                        ]),
                        Comment.canAdminComment(comment) ? m('.actions', [
                            (comment.isEdit ? [m('i.icon.icon-cancel', {onclick: function() {
                                comment.isEdit = false;
                            }, title: Strings.json.cancel}),
                                ' ',
                                m('i.icon.icon-floppy', {onclick: function() {
                                    comment.isEdit = false;
                                    Actions.editComment();
                                }, title: Strings.json.save})] :
                            m('i.icon.icon-edit', {onclick: function() {
                                comment.isEdit = true;
                                Boards.beginEditComment(comment);
                            }, title: Strings.json.boards_taskEditComment})),
                            ' ',
                            m('i.icon.icon-trash-empty', {onclick: function() {
                                Boards.confirmRemoveComment(comment);
                            }, title: Strings.json.boards_taskRemoveComment})
                        ]) : ''
                    ];
                })
                )
            ]) : '',
            m('.clearfix', [
                m('h3', Strings.json.boards_taskAddComment),
                m('textarea', {rows: 5, value: Modal.newComment, onchange: function(e) {
                    Modal.newComment = e.target.value;
                }}),
                m('button.right', {onclick: function() {
                    Actions.addComment();
                }}, [m('i.icon.icon-comment-empty'), ' ', Strings.json.boards_taskAddComment])
            ]),
            data.showActivity ? m('.activity' + (data.collapseActivity ? '.collapsed' : ''),
                m('.title', [
                    m('h2', [
                        'Task Activity',
                        m('span.right', [
                            data.collapseActivity ?
                            '' :
                            m('i.icon.icon-angle-double-down', {onclick: function() {
                                data.collapseActivity = true;
                            }}),
                            data.collapseActivity ?
                            m('i.icon.icon-angle-double-up', {onclick: function() {
                                data.collapseActivity = false;
                            }}) : ''
                        ])
                    ])
                ]),
                data.collapseActivity ? '' : m('.log-items', data.viewTaskActivities.map(function(item) {
                    return m('.log-item', [
                        item.text,
                        m('span', new Date(item.timestamp * 1000).toLocaleString())
                    ]);
                }))
            ) : ''
        ];
    }
};
