var m = require('mithril');

var Actions = require('../../models/actions.js');
var Boards = require('../../models/boards.js');
var Modal = require('../../models/modal.js');
var Strings = require('../../models/strings.js');

module.exports = {
    view: function(vnode) {
        return [
            m('label', [
                Strings.json.boards_taskTitle,
                m('input', {
                    type: 'text',
                    name: 'title',
                    placeholder: Strings.json.boards_taskTitlePlaceholder,
                    onchange: function(e) {
                        Modal.modalProps.title = e.target.value;
                    },
                    value: Modal.modalProps.title
                })
            ]),
            m('label', [
                Strings.json.boards_taskDescription,
                m('textarea', {
                    onchange: function(e) {
                        e.preventDefault();
                        Modal.modalProps.description = e.target.value;
                    },
                    name: 'description',
                    rows: 5,
                    placeholder: Strings.json.boards_taskDescriptionPlaceholder,
                    value: Modal.modalProps.description
                })
            ]),
            m('label', [
                Strings.json.boards_taskAssignees,
                m('select',
                    {name: 'assignees', multiple: true, onchange: Boards.updateTaskAssignees},
                    Boards.activeboard.users.map(function(user) {
                        var selected = false;
                        Modal.modalProps.assignees.forEach(function(assignee) {
                            selected = parseInt(assignee.id) === parseInt(user.id) || selected;
                        });
                        return m('option', {value: user.id, selected: selected}, user.username);
                    }))
            ]),
            m('label', [
                Strings.json.boards_taskCategories,
                m('select',
                    {name: 'categories', multiple: true, onchange: Boards.updateTaskColorByCategory},
                    Boards.activeboard.categories.map(function(category) {
                        var selected = false;
                        Modal.modalProps.categories.forEach(function(c) {
                            selected = parseInt(category.id) === parseInt(c.id) || selected;
                        });
                        return m('option',
                            {value: category.id, color: category.default_task_color, selected: selected},
                            category.name);
                    })
                )
            ]),
            m('.half', [
                m('label', [
                    Strings.json.boards_taskColumn,
                    m('select', {name: 'column', onchange: function(e) {
                        Modal.modalProps.column_id = parseInt(e.target.value);
                    }, value: Modal.modalProps.column_id}, Boards.activeboard.columns.map(function(column) {
                        return m('option', {value: column.data.id}, column.data.name);
                    })
                    )
                ]),
                m('label', [
                    Strings.json.boards_taskColor,
                    m('input', {type: 'color', value: Modal.modalProps.color, onchange: function(e) {
                        Modal.modalProps.color = e.target.value;
                    }})
                ])
            ]),
            m('.half', [
                m('label', [
                    Strings.json.boards_taskDateDue,
                    m('input', {type: 'date', onchange: function(e) {
                        Modal.modalProps.due_date = e.target.value;
                    }, value: Modal.modalProps.due_date})]),
                m('label', [
                    Strings.json.boards_taskPoints,
                    m('input', {type: 'number', onchange: function(e) {
                        Modal.modalProps.points = e.target.value;
                    }, value: Modal.modalProps.points})
                ])
            ]),
            m('.buttons', [
                Modal.modalProps.id === 0 ? m('button', {disabled: Boards.saving, onclick: function() {
                    Actions.addTask(Modal.modalProps);
                }}, [m('i.icon.icon-plus'), ' ', Strings.json.boards_addTask]) : '',
                Modal.modalProps.id !== 0 ? m('button', {disabled: Boards.saving, onclick: function() {
                    Actions.updateTask();
                }}, [m('i.icon.icon-floppy'), ' ', Strings.json.boards_saveTask]) : '',
                ' ',
                m('button.flat', {onclick: function() {
                    Modal.close(Modal.modals.edit);
                }}, Strings.json.cancel)
            ])
        ];
    }
};
