var m = require('mithril');

var Actions = require('../../models/actions.js');
var Modal = require('../../models/modal.js');
var Boards = require('../../models/boards.js');
var Strings = require('../../models/strings.js');

module.exports = {
    view: function(vnode) {
        return [
            m('.center', [
                Strings.json.boards_confirmWarning,
                m('br'),
                Strings.json.boards_confirmContinue
            ]),
            m('.buttons', [
                m('button.flat', {onclick: function() {
                    Modal.close(Modal.modals.removetask);
                    Actions.removeTask();
                }}, Strings.json.yes),
                ' ',
                m('button', {onclick: function() {
                    Modal.close(Modal.modals.removetask);
                }}, Strings.json.no)
            ])
        ];
    }
};
