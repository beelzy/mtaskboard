var m = require('mithril');

var Modal = require('../../models/modal.js');
var Settings = require('../../models/settings.js');
var Strings = require('../../models/strings.js');

module.exports = {
    view: function(vnode) {
        return [
            m('label', [
                Strings.json.settings_changeUsername,
                m('input', {value: Modal.modalProps.user.username, onchange: function(e) {
                    Modal.modalProps.user.username = e.target.value;
                }, type: 'text', name: 'new-username', placeholder: Strings.json.settings_username})
            ]),
            m('label', [
                Strings.json.settings_changePassword,
                m('input', {value: Modal.modalProps.user.password, onchange: function(e) {
                    Modal.modalProps.user.password = e.target.value;
                }, type: 'password', name: 'new-password', placeholder: Strings.json.settings_password})
            ]),
            m('input', {value: Modal.modalProps.user.password_verify, onchange: function(e) {
                    Modal.modalProps.user.password_verify = e.target.value;
            }, type: 'password', name: 'new-password-verify', placeholder: Strings.json.settings_verifyPassword}),
            m('label', [
                Strings.json.settings_changeEmail,
                m('input', {value: Modal.modalProps.user.email, onchange: function(e) {
                    Modal.modalProps.user.email = e.target.value;
                }, type: 'text', name: 'new-email', placeholder: Strings.json.settings_emailPlaceholder})
            ]),
            ' ',
            Settings.boards.length ? m('div', [
                m('label', [
                    Strings.json.settings_defaultBoard,
                    ' ',
                    m('i.icon.icon-help-circled', {'data-help': Strings.json.settings_defaultBoardHelp}),
                    m('select', {onchange: function(e) {
                        Modal.modalProps.user.default_board_id = e.target.value;
                    }}, [
                        m('option', {value: 0, selectd: Modal.modalProps.user.default_board_id === 0}, Strings.json.none),
                        Settings.boards.map(function(board) {
                            return m('option', {value: board.id, selected: Modal.modalProps.user.default_board_id === board.id}, board.name);
                        })
                    ])
                ]),
                ' ',
                m('label', [
                    Strings.json.settings_boardAccess,
                    m('select', {onchange: function(e) {
                        Settings.updateBoardAccess(e);
                    }, multiple: true},
                        Settings.boards.map(function(board) {
                            var selected = false;
                            Modal.modalProps.user.boardAccess.forEach(function(user) {
                                selected = parseInt(board.id) === parseInt(user) || selected;
                            });
                            return m('option', {value: board.id, selected: selected}, board.name);
                        })
                    )
                ])
            ]) : '',
            ' ',
            m('label', [
                Strings.json.settings_securityLevel,
                m('select', {name: 'new-security', onchange: function(e) {
                    Modal.modalProps.user.security_level = parseInt(e.target.value);
                }}, [
                    m('option', {value: 3, selected: Modal.modalProps.user.security_level === 3}, Strings.json.settings_user),
                    m('option', {value: 2, selected: Modal.modalProps.user.security_level === 2}, Strings.json.settings_boardAdmin),
                    m('option', {value: 1, selected: Modal.modalProps.user.security_level === 1}, Strings.json.settings_admin)
                ])
            ]),
            ' ',
            m('.buttons', [
                m('button', {onclick: function(e) {
                    Settings.addEditUser();
                }, disabled: Settings.saving}, [
                    m('i.icon' + (Modal.modalProps.prefix ? '.icon-plus' : '.icon-floppy')),
                    ' ',
                    Strings.json.settings_saveUser
                ]),
                ' ',
                m('button.flat', {onclick: function(e) {
                    Modal.close(Modal.modals.edituser);
                }}, Strings.json.cancel)
            ])
        ];
    }
};
