var m = require('mithril');

var Modal = require('../../models/modal.js');
var Settings = require('../../models/settings.js');
var Strings = require('../../models/strings.js');
var BoardAdmin = require('../../models/board-admin.js');
var User = require('../../models/user.js');
var InlineEdit = require('../inline-edit.js');
var DND = require('mithril-dnd');

module.exports = {
    view: function(vnode) {
        return [
            m('label', [
                Strings.json.settings_boardName,
                m('input', {type: 'text', onchange: function(e) {
                    Modal.modalProps.name = e.target.value;
                }, name: 'board-name', placeholder: 'Board Name', value: Modal.modalProps.name})
            ]),
            ' ',
            m('.half-modal', [
                m('label', Strings.json.settings_columns),
                m(DND.view, {groupid: 'addboard', hidden: !Modal.modalProps.columns.length, itemKey: function(column) {
                    return column.data.position;
                }, draggable: function(e) {
                    return e.target.classList.contains('icon-resize-vertical') ? e.target : null;
                }, selector: 'ul.modal-list.column-list', itemSelector: 'li', drop: function() {
                    Settings.drop();
                }, list: Modal.modalProps.columns, template: function(column) {
                    return [m('i.icon.icon-resize-vertical'),
                            m(InlineEdit, {text: column.data.name, save: function(text) {
                                column.data.name = text;
                            }}),
                            m('span.actions', m('i.icon.icon-trash-empty.color-secondary', {title: Strings.json.settings_removeColumn, onclick: function(e) {
                                e.preventDefault();
                                Modal.removeColumn(column);
                            }}))
                        ];
                }}),
                ' ',
                m('.quick-add', m('form', [
                    m('input', {type: 'text', onkeyup: function(e) {
                        Modal.modalProps.newColumnName = e.target.value;

                        if (e.key === 'Enter') {
                            Modal.addColumn(e);
                        }
                    }, value: Modal.modalProps.newColumnName, name: 'new-column', placeholder: Strings.json.settings_columnName}),
                    ' ',
                    m('button.flat', {type: 'submit', onclick: Modal.addColumn, title: Strings.json.settings_addColumn}, m('i.icon.icon-plus'))
                ]))
            ]),
            ' ',
            m('.half-modal', [
                m('label', Strings.json.settings_categories),
                Modal.modalProps.categories.length ? m('ul.modal-list.category-list',
                    Modal.modalProps.categories.map(function(category) {
                        return m('li', [
                            m('input', {type: 'color', value: BoardAdmin.getColor(category), title: Strings.json.settings_defaultTaskColor, onchange: function(e) {
                                BoardAdmin.setColor(e.target.value, category);
                            }}),
                            m(InlineEdit, {text: category.name, save: function(text) {
                                category.name = text;
                            }}),
                            m('span.actions', m('i.icon.icon-trash-empty.color-secondary', {onclick: function() {
                                Modal.removeCategory(category);
                            }}))
                        ]);
                    })
                ) : '',
                ' ',
                m('.quick-add.categories', m('form', [
                    m('input', {type: 'text', value: Modal.modalProps.newCategoryName, onkeyup: function(e) {
                        Modal.modalProps.newCategoryName = e.target.value;

                        /*
                        if (e.key === 'Enter') {
                            Modal.addCategory(e);
                        }
                        */
                    }, name: 'new-category', placeholder: Strings.json.settings_categoryName}),
                    ' ',
                    m('input', {type: 'color', value: Modal.modalProps.categoryDefaultColor, onchange: function(e) {
                        Modal.modalProps.categoryDefaultColor = e.target.value;
                    }, name: 'category-default-color', title: Strings.json.settings_defaultTaskColor}),
                    ' ',
                    m('button.flat', {type: 'submit', onclick: Modal.addCategory, title: Strings.json.settings_addCategory}, m('i.icon.icon-plus'))
                ]))
            ]),
            ' ',
            m('.clearfix'),
            ' ',
            m('.issue-trackers', [
                m('label', [
                    Strings.json.settings_issueTrackers,
                    ' ',
                    m('i.icon.icon-help-circled', {'data-help': Strings.json.settings_issueTrackersHelp})
                ]),
                ' ',
                Modal.modalProps.issue_trackers.length ? m('ul.modal-list',
                    Modal.modalProps.issue_trackers.map(function(tracker) {
                        return m('li.double-edit', [
                            m(InlineEdit, {classes: ['first'], text: tracker.url, save: function(text) {
                                tracker.url = text;
                            }}),
                            m(InlineEdit, {classes: ['last'], text: tracker.regex, save: function(text) {
                                tracker.regex = text;
                            }}),
                            m('span.actions', m('i.icon.icon-trash-empty.color-secondary', {onclick: function(e) {
                                Modal.removeIssueTracker(tracker);
                            }}))
                        ]);
                    })
                ) : '',
                ' ',
                m('div', m('form', [
                    m('input', {type: 'text', value: Modal.modalProps.issueTrackerUrl, onkeyup: function(e) {
                        Modal.modalProps.issueTrackerUrl = e.target.value;

                        if (e.key === 'Enter') {
                            Modal.addIssueTracker(e);
                        }
                    }, name: 'issue-tracker', placeholder: Strings.json.settings_issueTrackerUrl}),
                    m('input', {type: 'text', value: Modal.modalProps.issueTrackerBugId, onkeyup: function(e) {
                        Modal.modalProps.issueTrackerBugId = e.target.value;

                        if (e.key === 'Enter') {
                            Modal.addIssueTracker(e);
                        }
                    }, name: 'issue-tracker-bug-id', placeholder: Strings.json.settings_issueTrackerRegExp}),
                    m('button.flat', {type: 'submit', onclick: Modal.addIssueTracker, title: Strings.json.settings_addIssueTracker}, m('i.icon.icon-plus'))
                ]))
            ]),
            ' ',
            m('.users', [
                m('label', Strings.json.settings_selectUsers),
                m('.clearfix'),
                User.users.map(function(user) {
                    if (user.security_level > 1) {
                        return m('label.user-select', [
                            m('input', {type: 'checkbox', value: user.selected, onclick: function(e) {
                                user.selected = e.target.checked;
                            }}),
                            ' ',
                            user.username,
                            user.security_level === 2 ? m('span', '*') : ''
                        ]);
                    }
                }),
                ' ',
                m('div', [
                    BoardAdmin.hasBAUsers ? m('em', [
                        m('strong', '*'),
                        m.trust('&nbsp;'),
                        Strings.json.settings_boardAdminMessage,
                        m('br')]
                    ) : '',
                    m('em', Strings.json.settings_adminAccessMessage)
                ])
            ]),
            ' ',
            m('.buttons', [
                m('button', {disabled: Settings.saving, onclick: Settings.addEditBoard}, [
                    m('i.icon' + (Modal.modalProps.title === 'Add' ? '.icon-plus' : '.icon-floppy')),
                    ' ',
                    Modal.modalProps.title === 'Add' ? Strings.json.settings_addBoard : Strings.json.settings_saveBoard
                ]),
                ' ',
                m('button.flat', {onclick: function(e) {Modal.close(Modal.modals.editboard);}}, Strings.json.cancel)
            ])
        ];
    }
};
