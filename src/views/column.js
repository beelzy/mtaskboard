var m = require('mithril');

var Actions = require('../models/actions.js');
var Column = require('../models/column.js');
var User = require('../models/user.js');
var Boards = require('../models/boards.js');
var Task = require('./task.js');
var TaskModel = require('../models/task.js');
var Strings = require('../models/strings.js');
var DND = require('mithril-dnd');

module.exports = {
    view: function(vnode) {
        var data = vnode.attrs.data;
        var nearlimit = Column.hasTaskLimit(data)
            && data.data.task_limit - data.data.tasks.length === 0 ?
            '.near-limit' : '';
        var limitreached = Column.hasTaskLimit(data)
            && data.data.task_limit - data.data.tasks.length < 0 ?
            '.limit-reached' : '';
        return [
            m('h3' + nearlimit + limitreached, [
                data.collapseTasks ? '' : m('span.icon.icon-minus-squared-alt', {onclick: function() {
                    Column.toggleTaskCollapse(data);
                }, title: Strings.json.boards_collapseAllTasks}),
                data.collapseTasks ? m('span.icon.icon-plus-squared-alt', {onclick: function() {
                    Column.toggleTaskCollapse(data);
                }, title: Strings.json.boards_expandAllTasks}) : '',
                ' ',
                data.data.name,
                Column.hasTaskLimit(data) ?
                m('span.count', data.data.tasks.length / Column.hasTaskLimit(data)) : '',
                m('span.badge',
                    {title: Strings.json.boards_tasksInColumn},
                    data.data.tasks && data.data.tasks.length || 0),
                m('span.icon.icon-angle-double-up', {
                    title: Strings.json.boards_expandColumn,
                    onclick: function() {
                        Column.toggleCollapsed(data);
                    }
                }),
                m('span.right.icon.icon-angle-double-down', {
                    title: Strings.json.boards_collapseColumn,
                    onclick: function() {
                        Column.toggleCollapsed(data);
                    }
                }),
                User.isAnyAdmin() ? m('span.count-editor', [
                    m('i.icon.icon-hashtag',
                        {title: Strings.json.boards_editTaskLimit, onclick: function() {
                            Column.beginLimitEdit(data);
                        }}),
                    data.showLimitEditor ? m('.limit-editor', [
                        m('input', {type: 'number', min: 0, value: data.newlimit, onchange: function(e) {
                            data.newlimit = e.target.value;
                        }, title: '0 = ' + Strings.json.boards_noLimit}),
                        m('i.icon.icon-cancel.right',
                            {title: Strings.json.boards_limitCancel, onclick: function() {
                                Column.cancelLimitChanges(data);
                            }}),
                        m('i.icon.icon-floppy.primary.right',
                            {title: Strings.json.boards_limitSave, onclick: function() {
                                Actions.saveLimitChanges(data);
                            }})
                    ]) : ''
                ]) : '',
                m('span.sort-by', [
                    Strings.json.sortBy,
                    ' ',
                    m('select', {onchange: function(e) {
                        data.sortOption = e.target.value; Column.sortTasks(data);
                    }}, [
                        m('option',
                            {value: 'pos', selected: data.sortOption === 'pos'},
                            Strings.json.boards_sortByPosition),
                        m('option',
                            {value: 'due', selected: data.sortOption === 'due'},
                            Strings.json.boards_sortByDueDate),
                        m('option',
                            {value: 'pnt', selected: data.sortOption === 'pnt'},
                            Strings.json.boards_sortByPoints)
                    ])
                ])
            ]),
            m('.quick-add', [
                m('input', {type: 'text', onkeyup: function(e) {
                    data.quickAdd.title = e.target.value;
                    if (e.key === 'Enter') {
                        e.stopPropagation();
                        Actions.quickAddClicked(data);
                    }
                }, placeholder: Strings.json.boards_quickAdd, value: data.quickAdd.title}),
                ' ',
                m('button.flat', {title: Strings.json.boards_addTask, onclick: function() {
                    Actions.quickAddClicked(data);
                }}, m('i.icon.icon-plus'))
            ]),
            m(DND.view, {groupid: 'tasks-' + data.data.board_id, draggable: function(e) {
                return e.target.classList.contains('drag-handle') ? e.target : null;
            }, selector: '.tasks', itemSelector: '.task-container', onDrop: function(e, data) {
                Boards.drop(data);
            }, list: data.data.tasks, template: function(task) {
                return m(Task, {data: task, column: data});
            }
            })
        ];
    }
};
