var m = require('mithril');
var Authenticate = require('../utils/authenticate.js');
var Login = require('../models/login.js');
var Page = require('../models/page.js');

module.exports = {
    oninit: function() {
        Authenticate.authenticate().then(function(e) {
            m.route.set('/boards');
        }, function(e) {
            console.info('failure', e);
        });
    },
    view: function(vnode) {
        return m('.login', 
            [
                m('form', 
                    [
                        m('h1', 'Taskboard'),
                        m('input', {
                            type: 'text',
                            placeholder: 'Username',
                            title: 'Username',
                            name: 'username',
                            autofocus: true,
                            oninput: Login.updateUsername
                        }, ''),
                        m('input', {
                            type: 'password',
                            placeholder: 'Password',
                            title: 'Password',
                            name: 'password',
                            autofocus: true,
                            oninput: Login.updatePassword
                        }, ''),
                        m('label',
                            m('input', {
                                type: 'checkbox',
                                name: 'remember',
                                onclick: function(e) {Login.remember = e.target.checked;}
                            }), ' Remember Me'),
                        m('button', {type: 'submit', onclick: Login.submit}, 'Sign In')
                    ]
                ),
                m('p', 'v' + Page.version)
            ]
        )
    }
};
