var m = require('mithril');

var ContextMenu = require('../models/contextmenu.js');

module.exports = {
    view: function(vnode) {
        return m('tb-context-menu', ContextMenu.model ?
            m('.context-menu-container' + (ContextMenu.isOpen ? '.menu-open' : ''), {
                style: 'top: ' + ContextMenu.menu.y + '; left: ' + ContextMenu.menu.x + ';'
            }, Object.keys(ContextMenu.model).length > 0 ? ContextMenu.model : '') : '');
    }
};
