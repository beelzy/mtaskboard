var m = require('mithril');

var Strings = require('../models/strings.js');
var Modal = require('./modal/modal.js');
var ModalModel = require('../models/modal.js');
var AddBoard = require('./modal/addboard.js');
var RemoveBoard = require('./modal/removeboard.js');
var User = require('../models/user.js');
var Settings = require('../models/settings.js');
var BoardAdmin = require('../models/board-admin.js');

module.exports = {
    view: function(vnode) {
        return [
            m('section', [
                m('h2', Strings.json.settings_boardAdministration),
                m('.row', [
                    m('h3', Strings.json.settings_currentBoards),
                    Settings.boards.length ? m('.small.right.filters', [
                        m('label.inline.right', [
                            Strings.json.settings_showByUser,
                            ' ',
                            m('select.autosize', {onchange: function(e) {
                                BoardAdmin.userFilter = parseInt(e.target.value);
                                Settings.filterBoards();
                            }}, [
                                m('option', {value: -1, selected: BoardAdmin.userFilter === -1}, Strings.json.settings_anyUser),
                                User.users.map(function(user) {
                                    return m('option', {value: user.id, selected: parseInt(BoardAdmin.userFilter) === parseInt(user.id)}, user.username);
                                })
                            ])
                        ]),
                        ' ',
                        m('label.inline.right', [
                            Strings.json.settings_filterBy,
                            ' ',
                            m('select.autosize', {onchange: function(e) {
                                BoardAdmin.statusFilter = parseInt(e.target.value);
                                Settings.filterBoards();
                            }}, [
                                m('option', {value: -1, selected: parseInt(BoardAdmin.statusFilter) === -1}, Strings.json.settings_allBoards),
                                m('option', {value: 1, selected: parseInt(BoardAdmin.statusFilter) === 1}, Strings.json.settings_active),
                                m('option', {value: 0, selected: parseInt(BoardAdmin.statusFilter) === 0}, Strings.json.settings_inactive)
                            ])
                        ]),
                        ' ',
                        m('label.inline.right', [
                            Strings.json.sortBy,
                            ' ',
                            m('select.autosize', {onchange: function(e) {
                                BoardAdmin.sortFilter = e.target.value;
                                Settings.filterBoards();
                            }}, [
                                m('option', {value: 'name-asc', selected: BoardAdmin.sortFilter === 'name-asc'}, Strings.json.settings_name + ' (A-Z)'),
                                m('option', {value: 'name-desc', selected: BoardAdmin.sortFilter === 'name-desc'}, Strings.json.settings_name + ' (Z-A)'),
                                m('option', {value: 'id-desc', selected: BoardAdmin.sortFilter === 'id-desc'}, Strings.json.settings_creationNew),
                                m('option', {value: 'id-asc', selected: BoardAdmin.sortFilter === 'id-asc'}, Strings.json.settings_creationOld)
                            ])
                        ]),
                    ]) : '',
                    ' ',
                    m('table.alternating', [
                        m('thead', m('tr', [
                            m('th', Strings.json.settings_name),
                            m('th', Strings.json.settings_columns),
                            m('th', Strings.json.settings_categories),
                            m('th', Strings.json.settings_users),
                            m('th', Strings.json.settings_actions)
                        ])),
                        ' ',
                        BoardAdmin.loading ? '' : m('tbody', [
                            Settings.boards.length === 0 ? m('tr', m('td.center', {colspan: 5}, Settings.noBoardsMessage())) : '',
                            Settings.boards.map(function(board) {
                                if (!board.filtered) {
                                    return m('tr', [
                                        m('td', m('a', {oncreate: m.route.link, onupdate: m.route.link, href: '/boards/' + board.id}, board.name)),
                                        ' ',
                                        m('td', m('ul', board.columns.map(function(column) {
                                            return m('li', [
                                                column.data.name,
                                                m('span.badge', {title: 'Task Count'}, column.data.tasks ? column.data.tasks.length: 0)
                                            ]);
                                        })
                                        )),
                                        ' ',
                                        m('td', board.categories && board.categories.length ? m('ul',
                                            board.categories.map(function(category) {
                                                return m('li', [
                                                    category.name,
                                                    m('span.badge', {title: Strings.json.settings_defaultTaskColor, style: 'background-color:' + BoardAdmin.getColor(category) + ';'}, m.trust('&nbsp;')),
                                                ]);
                                            })
                                        ) : ''),
                                        ' ',
                                        m('td', m('ul',
                                            board.users.map(function(user) {
                                                return m('li', user.username);
                                            })
                                            )),
                                        ' ',
                                        m('td', [
                                            User.user && User.user.security_level < 3 ? m('span', [
                                                m('a.padded', {href: '', onclick: function(e) {
                                                    e.preventDefault();
                                                    Settings.showModal('Edit', board);
                                                }, title: Strings.json.settings_editBoard}, m('i.icon.icon-edit.color-primary')),
                                                m('a.padded', {href: '', onclick: function(e) {
                                                    e.preventDefault();
                                                    Settings.showConfirmModal(board.id);
                                                }, title: Strings.json.settings_removeBoard}, m('i.icon.icon-trash-empty.color-secondary')),
                                                m('label.padded', [
                                                    m('input.hidden', {type: 'checkbox', onclick: function(e) {
                                                        board.is_active = e.target.checked;
                                                        Settings.toggleBoardStatus(board);
                                                    }, checked: !!board.is_active}),
                                                    m('span.toggle'),
                                                    ' ',
                                                    board.is_active ? Strings.json.settings_active : Strings.json.settings_inactive
                                                ])
                                            ]) : ''
                                        ])
                                    ]);
                                }
                            })
                        ])
                    ]),
                    ' ',
                    BoardAdmin.loading ? m('.center', m('.spinner')) : '',
                    ' ',
                    User.user && User.user.security_level === 1 ? m('button', {onclick: function() {
                        Settings.showModal('Add');
                    }}, [
                        m('i.icon.icon-plus'),
                        ' ',
                        Strings.json.settings_addBoard
                    ]) : ''
                ])
            ]),
            m('tb-modal', m(Modal, {'modal-title': Strings.json.settings_confirmBoardRemoval, 'modal-id': BoardAdmin.MODAL_CONFIRM_ID, contents: RemoveBoard, modal: ModalModel.modals.removeboard})),
            ModalModel.modals.editboard.isOpen && Object.keys(ModalModel.modalProps).length > 0 ? m('tb-modal', m(Modal, {'modal-title': ModalModel.modalProps.title === 'Add' ? Strings.json.settings_addBoard : Strings.json.settings_editBoard, 'modal-id': BoardAdmin.MODAL_ID, contents: AddBoard, modal: ModalModel.modals.editboard})) : ''
        ];
    }
};
