var m = require('mithril');

var Actions = require('../../models/actions.js');
var Boards = require('../../models/boards.js');
var ContextMenu = require('../../models/contextmenu.js');
var Strings = require('../../models/strings.js');
var Modal = require('../../models/modal.js');

module.exports = {
    view: function(vnode) {
        var data = vnode.attrs.data ? vnode.attrs.data : {};
        var column = vnode.attrs.column ? vnode.attrs.column : {};
        return [
            m('.menu-item', {onclick: function(e) {
                e.stopPropagation();
                Actions.showViewModal(column, data);
                ContextMenu.closeAllMenus();
            }}, Strings.json.boards_viewTask),
            m('.menu-item', {onclick: function(e) {
                e.stopPropagation();
                Actions.showModal(column, data);
                ContextMenu.closeAllMenus();
            }}, Strings.json.boards_editTask),
            m('.menu-item', {onclick: function(e) {
                e.stopPropagation();
                Modal.data = column;
                Boards.confirmRemoveTask(data.id);
                ContextMenu.closeAllMenus();
            }}, Strings.json.boards_removeTask),
            (Boards.activeboard.columns && Boards.activeboard.columns.length > 1 ? [
                m('.menu-item.no-highlight', m('span', m('hr'))),
                m('.menu-item', [
                    Strings.json.boards_copyTaskTo,
                    ' ',
                    m('i.icon.icon-help-circled', {'data-help': Strings.json.boards_copyMoveHelp}),
                    m('select#boardsList' + data.id + Strings.json.boards_copyTaskTo.split(' ')[0],
                        {onchange: function(e) {
                            e.stopPropagation();
                            Actions.copyTaskToBoard(e, data);
                            ContextMenu.closeAllMenus();
                        }}, [
                        m('option', {value: 0}, Strings.json.boards_selectBoard),
                        Boards.boards.map(function(board) {
                            return m('option', {value: board.id}, board.name);
                        })
                    ])
                ]),
                m('.menu-item', [
                    Strings.json.boards_moveTaskTo,
                    ' ',
                    m('i.icon.icon-help-circled', {'data-help': Strings.json.boards_copyMoveHelp}),
                    m('select#boardsList' + data.id + Strings.json.boards_moveTaskTo.split(' ')[0],
                        {onchange: function(e) {
                            e.stopPropagation();
                            Actions.moveTaskToBoard(e, data);
                            ContextMenu.closeAllMenus();
                        }}, [
                        m('option', {value: 0}, Strings.json.boards_selectBoard),
                        Boards.boards.map(function(board) {
                            if (board.name !== Boards.activeboard.name) {
                                return m('option', {value: board.id}, board.name);
                            }
                        })
                    ])
                ])
            ] : ''),
            m('.menu-item.no-highlight', m('span', m('hr'))),
            m('.menu-item.no-highlight', [Strings.json.boards_moveTask + ': ',
                m('select#columnsList' + data.id, {onchange: function(e) {
                    e.stopPropagation();
                    Actions.moveColumn(e, data);
                    ContextMenu.closeAllMenus();
                }}, [
                    m('option', {value: 0}, Strings.json.boards_selectColumn),
                    Boards.activeboard.columns.map(function(column) {
                        if (column.data.id !== data.column_id) {
                            return m('option', {value: column.data.id}, column.data.name);
                        }
                    })
                ])
            ]),
            m('.menu-item.no-highlight', m('span', m('hr'))),
            m('.menu-item', {onclick: function(e) {
                e.stopPropagation();
                Actions.showModal(column);
                ContextMenu.closeAllMenus();
            }}, Strings.json.boards_addTask),
        ];
    }
};
