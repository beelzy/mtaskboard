var m = require('mithril');

var Actions = require('../../models/actions.js');
var ContextMenu = require('../../models/contextmenu.js');
var Strings = require('../../models/strings.js');

module.exports = {
    view: function(vnode) {
        var data = vnode.attrs.data ? vnode.attrs.data : {};
        return m('.menu-item', {onclick: function(e) {
            e.stopPropagation();
            Actions.showModal(data);
            ContextMenu.closeAllMenus();
        }}, Strings.json.boards_addTask);
    }
};
