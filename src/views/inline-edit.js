var m = require('mithril');

module.exports = function() {
    var beginEdit = function(vnode) {
        isDisplay = false;
        text = vnode.attrs.text;

        setTimeout(function() {
            vnode.dom.querySelector('input[type="text"]').focus();
        }, 100);
    };
    var editDone = function(vnode) {
        isDisplay = true;
        vnode.attrs.save(text);
    };
    var index = 0;
    var isDisplay = true;
    var text = '';
    return {
        onupdate: function(vnode) {
            text = text.length > 0 ? text : vnode.attrs.text;
        },
        view: function(vnode) {
            return m('inline-edit' + (vnode.attrs.classes && vnode.attrs.classes.length > 0 ? '.' + vnode.attrs.classes.join('.') : ''),
                m('.inline-edit', [
                    m('span', {hidden: !isDisplay}, [
                        vnode.attrs.text,
                        m('i.icon.icon-edit.color-primary', {title: 'Edit', onclick: function() {
                            beginEdit(vnode);
                        }})
                    ]),
                    m('span', {hidden: isDisplay}, [
                        m('input', {type: 'text', value: text, onkeyup: function(e) {
                            text = e.target.value;
                        }}),
                        m('i.icon.icon-floppy.color-primary', {title: 'Save', onclick: function() {
                            editDone(vnode);
                        }})
                    ])
                ])
            );
        }
    }
};
