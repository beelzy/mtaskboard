var m = require('mithril');

var AutoActions = require('../models/autoactions.js');
var Modal = require('./modal/modal.js');
var ModalModel = require('../models/modal.js');
var RemoveAutoAction = require('./modal/removeautoaction.js');
var Settings = require('../models/settings.js');
var User = require('../models/user.js');
var Strings = require('../models/strings.js');

module.exports = {
    view: function(vnode) {
        return [
            m('section', [
                m('h2', Strings.json.settings_automaticActions),
                ' ',
                m('.row', [
                    m('h3', Strings.json.settings_currentAction),
                    ' ',
                    m('table.alternating.no-bottom-margin', [
                        m('thead', m('tr', [
                            m('th', Strings.json.settings_board),
                            m('th', Strings.json.settings_trigger),
                            m('th', Strings.settings_action),
                            m('th', Strings.settings_remove)
                        ])),
                        ' ',
                        AutoActions.loading ? '' : m('tbody', [
                            Settings.actions && Settings.actions.length === 0 ? m('tr', m('td.center', {colspan: 4}, m.trust(Settings.noActionsMessage()))) : '',
                            Settings.actions.map(function(action) {
                                return m('tr', [
                                    m('td', Settings.getBoardName(action.board_id)),
                                    m('td', Settings.getTriggerDescription(action)),
                                    m('td', Settings.getTypeDescription(action)),
                                    m('td', User.user && User.user.security_level < 3 ? m('span', m('a', {title: Strings.json.settings_removeAutoAction, href: '', onclick: function(e) {
                                        e.preventDefault();
                                        Settings.showActionConfirmModal(action);
                                    }}, m('i.icon.icon-trash-empty.color-secondary'))) : '')
                                ]);
                            })
                        ])
                    ]),
                    ' ',
                    !AutoActions.loading && AutoActions.hasInactiveBoards ? m('div', m('em', '* ' + Strings.json.settings_inactiveBoardMessage)) : '',
                    ' ',
                    AutoActions.loading ? m('.center', m('.spinner')) : ''
                ]),
                User.user && User.user.security_level < 3 ? m('.row', [
                    m('h3', Strings.settings_addAction),
                    ' ',
                    m('table', [
                        m('thead', m('tr', [
                            m('th', Strings.json.settings_selectBoard),
                            m('th', Strings.json.settings_selectTrigger),
                            m('th', Strings.json.settings_selectAction)
                        ])),
                        ' ',
                        m('tbody', [
                            m('tr.borderless', [
                                m('td', m('select', {onchange: function(e) {
                                    AutoActions.newAction.board_id = e.target.value;
                                    Settings.updateTriggerSources();
                                }}, [
                                    m('option', {value: null, selected: AutoActions.newAction.board_id === null}, Strings.json.settings_selectBoard),
                                    Settings.boards.map(function(board) {
                                        return m('option', {value: board.id, selected: AutoActions.newAction.board_id === board.id}, board.name);
                                    })
                                ])),
                                m('td', m('select', {disabled: AutoActions.newAction.board_id === null, onchange: function(e) {
                                    AutoActions.newAction.trigger = parseInt(e.target.value);
                                    Settings.updateTriggerSources();
                                }},
                                        AutoActions.triggers().map(function(trigger) {
                                            return m('option', {value: trigger[0], selected: AutoActions.newAction.trigger === trigger[0]}, trigger[1]);
                                        })
                                )),
                                m('td', m('select', {disabled: AutoActions.newAction.board_id === null, onchange: function(e) {
                                    AutoActions.newAction.type = parseInt(e.target.value);
                                    Settings.updateActionSources();
                                }},
                                    AutoActions.types().map(function(type) {
                                        return m('option', {value: type[0], selected: AutoActions.newAction.type === type[0]}, type[1]);
                                    })
                                ))
                            ]),
                            ' ',
                            m('tr.borderless', [
                                m('td'),
                                m('td', AutoActions.triggerSources.length ? m('select', {disabled: AutoActions.newAction.board_id === null, onchange: function(e) {
                                    AutoActions.newAction.source_id = parseInt(e.target.value);
                                    AutoActions.checkAddDisabled();
                                }},
                                    AutoActions.triggerSources.map(function(source) {
                                        return m('option', {value: source[0], selected: AutoActions.newAction.source_id === source[0]}, source[1]);
                                    })
                                ) : ''),
                                m('td', [
                                    AutoActions.newAction.type > 1 && AutoActions.newAction.type < 6 ? m('select', {disabled: AutoActions.newAction.board_id === null, onchange: function(e) {
                                        AutoActions.newAction.change_to = parseInt(e.target.value);
                                        AutoActions.checkAddDisabled();
                                    }},
                                        AutoActions.actionSources.map(function(source) {
                                            return m('option', {value: source[0], selected: AutoActions.newAction.change_to === source[0]}, source[1]);
                                        })
                                    ) : '',
                                    AutoActions.newAction.type == 1 ? m('input', {disabled: AutoActions.board_id === null, type: 'color', name: 'change-to-color', value: AutoActions.newAction.change_to, onchange: function(e) {
                                        AutoActions.newAction.change_to = e.target.value;
                                        AutoActions.checkAddDisabled();
                                    }}) : ''
                                ])
                            ])
                        ])
                    ]),
                    ' ',
                    m('button.right', {disabled: AutoActions.isAddDisabled, onclick: Settings.addNewAction}, [
                        m('i.icon.icon-plus'),
                        ' ',
                        Strings.json.settings_addAction
                    ])
                ]) : ''
            ]),
            m('tb-modal', m(Modal, {'modal-title': 'Confirm Automatic Action Removal', 'modal-id': Modal.MODAL_CONFIRM_ID, contents: RemoveAutoAction, modal: ModalModel.modals.removeautoaction}))
        ];
    }
};
