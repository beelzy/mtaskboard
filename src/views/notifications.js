var m = require('mithril');
var Notifications = require('../models/notifications.js');

module.exports = {
    view: function(vnode) {
        return m('tb-notifications', m('.notifications', Notifications.notes.map(function(note, i) {
            return m('.' + note.type, {onclick: Notifications.removeNote, index: i}, note.text);
        })));
    }
};
