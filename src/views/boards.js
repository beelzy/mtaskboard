var m = require('mithril');

var Actions = require('../models/actions.js');
var Request = require('../utils/request.js');
var TopNav = require('./topnav.js');
var Boards = require('../models/boards.js');
var Column = require('./column.js');
var User = require('../models/user.js');
var Strings = require('../models/strings.js');
var ContextMenu = require('../models/contextmenu.js');
var ColumnMenu = require('./contextmenu/column.js');
var DND = require('mithril-dnd');

var showBoard = false;
module.exports = {
    oninit: function(vnode) {
        Actions.checkAuthentication(true);
    },
    onupdate: function(vnode) {
        Actions.checkAuthentication(false);
    },
    view: function(vnode) {
        var doublerow = User.options
            && User.options.multiple_tasks_per_row
            && parseInt(User.options.multiple_tasks_per_row) !== 0;
        return Boards.showBoard ? [m('tb-top-nav', m(TopNav, {pagename: Strings.json.boards})),
            m('.board-nav', [
                Boards.boards && Boards.boards.length ? m('label', [
                    Strings.json.boards_selectBoard,
                    ' ',
                    m('select', {onchange: function(e) {
                        Boards.goToBoard(e.target.value);
                    }}, [
                        m('option', {value: ''}, Strings.json.boards_selectBoard),
                        Boards.boards.map(function(board) {
                            return m('option',
                                {value: board.id, selected: board.id === Boards.boardNavId}, board.name);
                        })
                    ])
                ]) : '',
                Boards.activeboard ? m('.right', [
                    m('label', [
                        Strings.json.boards_hideFiltered,
                        ' ',
                        m('input', {type: 'checkbox', onchange: Boards.toggleFiltered})]),
                    ' ',
                    m('label', [
                        Strings.json.boards_userFilter,
                        ' ',
                        m('select', {onchange: function(e) {
                            Boards.userFilter = parseInt(e.target.value);
                            Boards.filterTasks();
                        }}, [
                            m('option',
                                {value: 'null', selected: Boards.userFilter === null},
                                Strings.json.boards_filterByAny),
                            m('option',
                                {value: -1, selected: Boards.userFilter === -1},
                                Strings.json.boards_filterByUnassigned),
                            Boards.activeboard.users ? Boards.activeboard.users.map(function(user) {
                                return m('option',
                                    {value: user.id, selected: Boards.userFilter === user.id},
                                    user.username);
                            }) : ''
                        ])
                    ]),
                    ' ',
                    m('label', [
                        Strings.json.boards_categoryFilter,
                        ' ',
                        m('select', {onchange: function(e) {
                            Boards.categoryFilter = parseInt(e.target.value);
                            Boards.filterTasks();
                        }}, [
                            m('option',
                                {value: 'null', selected: Boards.categoryFilter === null},
                                Strings.json.boards_filterByAny),
                            m('option',
                                {value: -1, selected: Boards.categoryFilter === -1},
                                Strings.json.boards_filterByUncategorized),
                            Boards.activeboard.categories ?
                            Boards.activeboard.categories.map(function(category) {
                                return m('option',
                                    {value: category.id, selected: Boards.categoryFilter === category.id},
                                    category.name);
                            }) : ''
                        ])
                    ])
                ]) : ''
            ]),
            !Boards.noBoards() && Boards.activeboard && User.user.default_board_id ?
            '' :
            m('.no-boards.center', [
                m('h1', Strings.json.boards_noBoards),
                m('p', [
                    Strings.json.boards_noDefaultMessage,
                    ' ',
                    m('a', {oncreate: m.route.link, href: '/settings'}, Strings.json.settings)
                ]),
                m('p')
            ]),
            m('.board',
                Boards.activeboard && Boards.activeboard.hasOwnProperty('columns')
                ? Boards.activeboard.columns.map(function(column) {
                    return m('tb-column.column'
                        + (column.isCollapsed ? '.collapsed' : '')
                        + (doublerow ? '.double' : ''),
                        {oncontextmenu: function(e) {
                            ContextMenu.open(e, m(ColumnMenu, {data: column}));
                        }}, m(Column, { data: column}));
                }) : ''),
            m(DND.mirror)
        ] : '';
    }
};
