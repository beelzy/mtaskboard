var m = require('mithril');
var marked = require('marked');
var Actions = require('../models/actions.js');
var Task = require('../models/task.js');
var User = require('../models/user.js');
var ContextMenu = require('../models/contextmenu.js');
var TaskMenu = require('./contextmenu/task.js');
var Strings = require('../models/strings.js');
var Boards = require('../models/boards.js');

module.exports = {
    view: function(vnode) {
        var data = vnode.attrs.data;
        var column = vnode.attrs.column;
        var textcolor = Task.getTextColor(data.color);
        return m('.task' + (data.filtered ? '.filtered' : '') + (data.hideFiltered ? '.hide' : ''), {
            style: 'background-color: ' + data.color + '; color: ' + textcolor + ';',
            ondblclick: function() {
                Actions.showViewModal(column, data);
            },
            oncontextmenu: function(e) {
                ContextMenu.open(e, m(TaskMenu, {data: data, column: column}));
            }
        }, [
            m('h4.drag-handle', [
                m('span.icon' + (data.isCollapsed ? '.icon-plus-squared-alt' : '.icon-minus-squared-alt'),
                    {style: 'color: ' + textcolor,
                        title: (data.isCollapsed ?
                            Strings.json.boards_expandTask :
                            Strings.json.boards_collapseTask),
                        onclick: function() {
                            data.isCollapsed = !data.isCollapsed;
                        }
                    }),
                ' ',
                data.title,
                data.points > 0 ? m('span.badge.right',
                    {title: Strings.json.boards_taskPoints},
                    data.points) : ''
            ]),
            !data.isCollapsed ? m('.description', m.trust(Boards.getTaskDescription(data))) : '',
            m('.stats', [
                User.options && parseInt(User.options.show_assignee) ? m('span', [
                    Strings.json.boards_taskAssignedTo,
                    ' ',
                    data.assignees.map(function(assignee) {
                        return [m('span', assignee.username), ' '];
                    }),
                    !data.assignees || !data.assignees.length ?
                    m('span', Strings.json.boards_taskUnassigned) : ''
                ]) : '',
                m('span.right', [
                    data.due_date ?
                    m('span' + (data.isOverDue ? '.overdue' : '') + (data.isNearlyDue ? '.near-due' : ''),
                        Strings.json.boards_taskDue
                        + ': ' + new Date(data.due_date).toLocaleDateString()) : '',
                    data.comments.length > 0 ? m('span.icon.icon-chat-empty',
                        {title: data.comments.length + ' ' + Strings.json.boards_taskComments}
                    ) : '',
                    data.attachments.length > 0 ? m('span.icon.icon-attach',
                        {title: data.attachments.length + ' ' + Strings.json.boards_taskAttachments}
                    ) : '',
                    data.categories.length > 0 ? data.categories.map(function(category) {
                        return m('span.category', {title: Strings.json.boards_taskCategory}, category.name);
                    }) : ''
                ]),
                data.percentComplete ? m('div', Task.getPercentTitle(data)) : ''
            ])
        ]);
    }
};
