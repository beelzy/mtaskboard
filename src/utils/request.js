var m = require('mithril');

var mimeType = function(xhr) {
    xhr.overrideMimeType('application/json');
};

var setToken = function(response) {
    if (response.data) {
        window.localStorage.setItem(Request.key, response.data[0]);
    }
};

var Request = {
    key: 'taskboard.jwt',
    DOMAIN: "",
    strings: function(language) {
        return m.request({
            method: 'GET',
            url: Request.DOMAIN + 'strings/' + language + '.json',
            config: mimeType
        });
    },
    get: function(url) {
        return m.request({
            method: 'GET',
            url: url,
            headers: {Authorization: window.localStorage.getItem(Request.key)},
            config: mimeType
        }).then(function(response, a, b) {
            setToken(response);
            return Promise.resolve(response);
        }, function(e) {
            Request.invalidate(e);
            return Promise.reject(e);
        });
    },
    post: function(url, data) {
        return m.request({
            method: 'POST',
            url: url,
            data: data,
            headers: {Authorization: window.localStorage.getItem(Request.key)},
            config: mimeType
        }).then(function(response) {
            setToken(response);
            return Promise.resolve(response);
        }, function(e) {
            Request.invalidate(e);
            return Promise.reject(e);
        });
    },
    deleteRequest: function(url) {
        return m.request({
            method: 'DELETE',
            url: url,
            headers: {Authorization: window.localStorage.getItem(Request.key)},
            config: mimeType
        }).then(function(response) {
            setToken(response);
            return Promise.resolve(response);
        }, function(e) {
            Request.invalidate(e);
            return Promise.reject(e);
        });
    },
    invalidate: function(e) {
        console.error(e);
        if (e.code === 400 || e.code === 401) {
            m.route.set('/');
            window.localStorage.removeItem(Request.key);
        }
    },
    refreshToken: function() {
        return this.post(Request.DOMAIN + 'api/refresh', {});
    },
    login: function(username, password, remember) {
        return this.post(Request.DOMAIN + 'api/login', {
            username: username, password: password, remember: remember
        }).then(
            function(result) {
                return Promise.resolve(result);
            }, function(result) {
                return Promise.resolve(result.response);
            });
    },
    logout: function() {
        return this.post(Request.DOMAIN + 'api/logout');
    },
    getBoards: function() {
        return this.get(Request.DOMAIN + 'api/boards');
    },
    editBoard: function(board) {
        return this.post(Request.DOMAIN + 'api/boards/' + board.id, board);
    },
    addBoard: function(board) {
        return this.post(Request.DOMAIN + 'api/boards', board);
    },
    removeBoard: function(boardid) {
        return this.deleteRequest(Request.DOMAIN + 'api/boards/' + boardid);
    },
    getUsers: function() {
        return this.get(Request.DOMAIN + 'api/users');
    },
    toggleCollapsed: function(userid, colid) {
        return this.post(Request.DOMAIN + 'api/users/' + userid + '/cols', {id: colid});
    },
    editUser: function(data, id) {
        return this.post(Request.DOMAIN + 'api/users/' + id, data);
    },
    editUserOptions: function(data, id) {
        return this.post(Request.DOMAIN + 'api/users/' + id + '/opts', data);
    },
    addUser: function(user) {
        return this.post(Request.DOMAIN + 'api/users', user);
    },
    removeUser: function(userid) {
        return this.deleteRequest(Request.DOMAIN + 'api/users/' + userid);
    },
    updateColumn: function(column) {
        return this.post(Request.DOMAIN + 'api/columns/' + column.id, column);
    },
    addTask: function(task) {
        return this.post(Request.DOMAIN + 'api/tasks', task);
    },
    updateTask: function(task) {
        return this.post(Request.DOMAIN + 'api/tasks/' + task.id, task);
    },
    removeTask: function(taskid) {
        return this.deleteRequest(Request.DOMAIN + 'api/tasks/' + taskid);
    },
    getTaskActivity: function(taskid) {
        return this.get(Request.DOMAIN + 'api/activity/task/' + taskid);
    },
    updateComment: function(comment) {
        return this.post(Request.DOMAIN + 'api/comments/' + comment.id, comment);
    },
    removeComment: function(commentid) {
        return this.deleteRequest(Request.DOMAIN + 'api/comments/' + commentid);
    },
    getActions: function() {
        return this.get(Request.DOMAIN + 'api/autoactions');
    },
    addAction: function(action) {
        return this.post(Request.DOMAIN + 'api/autoactions', action);
    },
    removeAction: function(actionid) {
        return this.deleteRequest(Request.DOMAIN + 'api/autoactions/' + actionid);
    }
};

module.exports = Request;
