var m = require('mithril');

var User = require('../models/user.js');
var Request = require('./request.js');

var Authenticate  = {
    authenticate: function() {
        return Request.post(Request.DOMAIN + 'api/authenticate').then(function(response) {
            User.updateUser(response.data[1], response.data[2]);
            return Promise.resolve(response);
        });
    }
};

module.exports = Authenticate;
