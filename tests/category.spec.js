var o = require('mithril/ospec/ospec');

global.window = require('mithril/test-utils/browserMock.js')();
global.document = window.document;

var Category = require('../src/models/category.js');
var Mock = require('./mock.js');

o.spec('Category', function() {
    o('creates a category from data', function() {
        var category = Category.fromData(Mock().categories[0]);

        o(category.id).equals(1);
        o(category.name).equals('Category 1');
        o(category.board_id).equals(1);
        o(category.default_task_color).equals('#ffffe0');
    });
});
