var o = require('mithril/ospec/ospec');

global.window = require('mithril/test-utils/browserMock.js');
global.document = window.document;

var Task = require('../src/models/task.js');

o.spec('Task', function() {
    o('checks a task\'s due date', function() {
        var task = Task.empty();
        task.due_date = new Date();

        Task.checkDueDate(task);
        o(task.isOverDue).equals(undefined);
        o(task.isNearlyDue).equals(true);

        task.due_date -= 24;

        Task.checkDueDate(task);
        o(task.isOverDue).equals(true);
    });

    o('sets the description text color', function() {
        o(Task.getTextColor('#ffffe0')).equals('#333333');
        o(Task.getTextColor('#000000')).equals('#efefef');
    });

    o('creates an empty task', function() {
        var task = Task.empty();

        o(task.id).equals(0);
        o(task.title).equals('');
        o(task.description).equals('');
        o(task.color).equals('#ffffe0');
        o(task.due_date).equals('');
        o(task.points).equals(0);
        o(task.position).equals(0);
        o(task.column_id).equals(0);
        o(task.filtered).equals(false);
        o(task.hideFiltered).equals(false);

        o(task.comments.length).equals(0);
        o(task.attachments.length).equals(0);
        o(task.assignees.length).equals(0);
        o(task.categories.length).equals(0);
    });
});
