var o = require('mithril/ospec/ospec');

global.window = require('mithril/test-utils/browserMock.js')();
global.document = window.document;

var Actions = require('../src/models/actions.js');
var Modal = require('../src/models/modal.js');
var Task = require('../src/models/task.js');
var Comment = require('../src/models/comment.js');
var Mock = require('./mock.js');

o.spec('Actions', function() {
    o('creates a task from data', function() {
        var task = Actions.taskFromData(Mock().tasks[0]);
        o(task.id).equals(1);
        o(task.title).equals('Task Title');
        o(task.description).equals('Task Description');
        o(task.color).equals('#ffffe0');
        o(task.due_date).equals('');
        o(task.points).equals(0);
        o(task.position).equals(1);
        o(task.column_id).equals(1);
        o(task.filtered).equals(false);
        o(task.hideFiltered).equals(false);
        o(task.comments.length > 0).equals(true);
        o(task.attachments.length).equals(0);
        o(task.assignees.length > 0).equals(true);
        o(task.categories.length > 0).equals(true);
    });
    o('creates a new empty column', function() {
        var newcolumn = Actions.newColumn();

        o(newcolumn.collapseTasks).equals(false);
        o(newcolumn.sortOption).equals('pos');
        o(newcolumn.showLimitEditor).equals(false);
        o(newcolumn.isCollapsed).equals(false);
        o(newcolumn.newlimit).equals(0);
        o(newcolumn.viewTaskActivities.length).equals(0);
        o(newcolumn.hasOwnProperty('data')).equals(true);
        o(newcolumn.hasOwnProperty('quickAdd')).equals(true);
    });
    o('creates a column from data', function() {
        var column = Actions.columnFromData(Mock().columns[0]);

        o(Object.keys(column.data).length > 0).equals(true);
        o(column.data.id).equals(1);
        o(column.data.name).equals('Column 1');
        o(column.data.position).equals(3);
        o(column.data.board_id).equals(1);
        o(column.data.task_limit).equals(0);
        o(column.isCollapsed).equals(false);
        o(column.data.tasks.length > 0).equals(true);
    });
    o('creates a board from data', function() {
        var board = Actions.boardFromData(Mock().board);
        o(board.id).equals('1');
        o(board.name).equals('Test');
        o(board.is_active).equals(1);
        o(board.users.length > 0).equals(true);
        o(board.categories.length > 0).equals(true);
    });
    o('displays the correct modal', function() {
        document = window.document;
        document.body.classList = {add: function() {}, remove: function() {}};
        Actions.updateActiveBoard(Mock().board);
        var column = Actions.columnFromData(Mock().columns[0]); 
        var task = Actions.taskFromData(Mock().tasks[0]);
        Actions.showModal(column);

        o(Modal.modalProps.id).equals(0);
        o(Modal.modals.edit.isOpen).equals(true);
        Actions.showModal(column, task);

        o(Modal.modalProps.id).equals(1);
    });
    o('updates a task\'s comments', function() {
        var comments = Mock().comments.map(function(comment) {return Comment.fromData(comment);});
        var task = Task.empty();
        Actions.updateTaskComments(task, comments);

        o(Object.keys(Modal.viewModalProps).length > 0).equals(true);
        o(task.comments.length > 0).equals(true);
    });
});
