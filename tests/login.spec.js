var o = require('mithril/ospec/ospec');

global.window = require('mithril/test-utils/browserMock.js')();
global.document = window.document;

var Login = require('../src/models/login.js');

o.spec('Login', function() {
    o('updates the username input field', function() {
        Login.updateUsername({target: {value: 'user'}});
        o(Login.username).equals('user');
    });
    o('updates the password input field', function() {
        Login.updatePassword({target: {value: 'password'}});
        o(Login.password).equals('password');
    });
});
