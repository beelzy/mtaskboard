var o = require('mithril/ospec/ospec');

global.window = require('mithril/test-utils/browserMock.js')();
global.document = window.document;

var Modal = require('../src/models/modal.js');

o.spec('Modal', function() {
    o('opens the modal', function() {
        document.body.classList = {add: function() {}, remove: function() {}};

        Modal.open(Modal.modals.edit);
        o(Modal.modals.edit.isOpen).equals(true);
    });

    o('closes the modal', function() {
        Modal.open(Modal.modals.edit);
        Modal.close(Modal.modals.edit);

        o(Modal.modals.edit.isOpen).equals(false);

        Modal.open(Modal.modals.removetask);
        Modal.close(Modal.modals.removetask, true);

        o(Modal.modals.removetask.isOpen).equals(true);
    });
});
