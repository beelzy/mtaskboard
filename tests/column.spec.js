var o = require('mithril/ospec/ospec');

global.window = require('mithril/test-utils/browserMock.js')();
global.document = window.document;

var Column = require('../src/models/column.js');
var Actions = require('../src/models/actions.js');
var Mock = require('./mock.js');

o.spec('Column', function() {
    o('checks that the column has a task limit', function() {
        var column = Actions.columnFromData(Mock().columns[0]);
        o(Column.hasTaskLimit(column)).equals(false);
    });

    o('sorts tasks in a column by pos', function() {
        var column = Actions.columnFromData(Mock().columns[0]);

        Column.sortTasks(column);

        var i = 0;
        column.data.tasks.forEach(function(task) {
            o(task.position >= i).equals(true);
            i = task.position;
        });
    });

    o('sorts tasks in a column by due date', function() {
        var column = Actions.columnFromData(Mock().columns[0]);

        column.sortOption = 'due';
        Column.sortTasks(column);

        var i = 0;
        column.data.tasks.forEach(function(task) {
            o(task.due_date >= i).equals(true);
            i = task.position;
        });
    });

    o('sorts tasks in a column by points', function() {
        var column = Actions.columnFromData(Mock().columns[0]);

        column.sortOption = 'pnt';
        Column.sortTasks(column);

        var i = column.data.tasks[0].points;
        column.data.tasks.forEach(function(task) {
            o(task.points <= i).equals(true);
            i = task.points;
        });
    });

    o('toggle task collapse', function() {
        var column = Actions.columnFromData(Mock().columns[0]);

        o(column.collapseTasks).equals(false);
        o(column.data.tasks[0].isCollapsed).equals(undefined);

        Column.toggleTaskCollapse(column);

        o(column.collapseTasks).equals(true);
        column.data.tasks.forEach(function(task) {
            o(task.isCollapsed).equals(true);
        });

        Column.toggleTaskCollapse(column);

        o(column.collapseTasks).equals(false);

        column.data.tasks.forEach(function(task) {
            o(task.isCollapsed).equals(false);
        });
    });

    o('begin editing limit', function() {
        var column = Actions.columnFromData(Mock().columns[0]);

        o(column.showLimitEditor).equals(false);

        Column.beginLimitEdit(column);

        o(column.showLimitEditor).equals(true);
    });

    o('cancel editing limit', function() {
        var column = Actions.columnFromData(Mock().columns[0]);

        Column.beginLimitEdit(column);
        Column.cancelLimitChanges(column);

        o(column.showLimitEditor).equals(false);
    });
});
