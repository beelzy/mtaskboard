var o = require('mithril/ospec/ospec');

global.window = require('mithril/test-utils/browserMock.js')();
global.document = window.document;

var Mock = require('./mock.js');
var Boards = require('../src/models/boards.js');
var Page = require('../src/models/page.js');
var Actions = require('../src/models/actions.js');
var Modal = require('../src/models/modal.js');

o.spec('Boards', function() {
    o('sets the active board', function() {
        Actions.updateActiveBoard(Mock().board);
        Boards.setActiveBoard();
        o(Boards.activeboard).equals(null);
        Boards.boardNavId = '1';
        Actions.updateActiveBoard(Mock().board);
        Boards.boards.push(Boards.activeboard);

        Boards.setActiveBoard();
        o(Boards.boards.activeboard).notEquals(null);
        o(Page.pagename).equals('Test');
    });
    o('clears all board data', function() {
        Actions.updateActiveBoard(Mock().board);
        Boards.boardNavId = '1';
        Boards.boards.push(Boards.activeboard);
        o(Object.keys(Boards.activeboard).length > 0).equals(true);
        o(Boards.boards.length > 0).equals(true);

        Boards.clearBoards();
        o(Boards.boardNavId).equals(null);
        o(Object.keys(Boards.activeboard).length).equals(0);
        o(Boards.boards.length).equals(0);
    }); 
    o('sorts board columns', function() {
        var board = Actions.boardFromData(Mock().board);
        Boards.sortBoard(board);

        var i = 0;
        board.columns.forEach(function(column) {
            o(parseInt(column.data.position) >= i).equals(true);
            i = parseInt(column.data.position) + 1;
        });
    });
    o('checks if there are any boards', function() {
        o(Boards.noBoards()).equals(true);

        Actions.updateActiveBoard(Mock().board);
        Boards.boards.push(Boards.activeboard);

        o(Boards.noBoards()).equals(false);
    });
});

o.spec('Task Filters', function() {
    o('toggles board filters', function() {
        Actions.updateActiveBoard(Mock().board);

        o(Boards.hideFiltered).equals(false);
        o(Boards.activeboard.columns[0].data.tasks[0].hideFiltered).equals(false);

        Boards.toggleFiltered();
        o(Boards.hideFiltered).equals(true);
        o(Boards.activeboard.columns[0].data.tasks[0].hideFiltered).equals(true);
    });
    o('filters tasks', function() {
        Actions.updateActiveBoard(Mock().board);

        Boards.filterTasks();
        o(Boards.activeboard.columns[0].data.tasks[0].filtered).equals(false);
    });

    o('filter tasks unassigned', function() {
        Actions.updateActiveBoard(Mock().board);

        Boards.userFilter = -1;
        Boards.filterTasks();
        o(Boards.activeboard.columns[1].data.tasks[0].filtered).equals(true);
        o(Boards.activeboard.columns[1].data.tasks[1].filtered).equals(false);
    });

    o('filter tasks user id = 1', function() {
        Actions.updateActiveBoard(Mock().board);

        Boards.userFilter = 1;
        Boards.filterTasks();
        o(Boards.activeboard.columns[1].data.tasks[0].filtered).equals(false);
        o(Boards.activeboard.columns[1].data.tasks[1].filtered).equals(true);
    });

    o('filter tasks no category', function() {
        Actions.updateActiveBoard(Mock().board);

        Boards.userFilter = 0;
        Boards.categoryFilter = -1;
        Boards.filterTasks();
        o(Boards.activeboard.columns[1].data.tasks[0].filtered).equals(true);
        o(Boards.activeboard.columns[0].data.tasks[0].filtered).equals(false);
    });

    o('filter tasks category id = 1', function() {
        Actions.updateActiveBoard(Mock().board);

        Boards.userFilter = 0;
        Boards.categoryFilter = 1;
        Boards.filterTasks();
        o(Boards.activeboard.columns[1].data.tasks[0].filtered).equals(false);
        o(Boards.activeboard.columns[0].data.tasks[0].filtered).equals(true);
    });

    o('filter tasks by category id = 1 and user id = 1', function() {
        Actions.updateActiveBoard(Mock().board);

        Boards.userFilter = 1;
        Boards.categoryFilter = 1;
        Boards.filterTasks();
        o(Boards.activeboard.columns[1].data.tasks[0].filtered).equals(false);
        o(Boards.activeboard.columns[1].data.tasks[1].filtered).equals(true);
        o(Boards.activeboard.columns[0].data.tasks[0].filtered).equals(true);
    });
});

o.spec('Modal Functions', function() {
    o('select task assignees', function() {
        Actions.updateActiveBoard(Mock().board);
        var e = {target: {selectedOptions: [{value: '1'}]}};

        Boards.updateTaskAssignees(e);
        o(Modal.modalProps.assignees.length > 0).equals(true);
    });

    o('get current username', function() {
        Actions.updateActiveBoard(Mock().board);

        var username = Boards.getUserName(Mock().user.id);
        o(username).equals('user');
    });

    o('open remove task modal', function() {
        var task = Mock().tasks[0].id;
        Boards.confirmRemoveTask(task);

        o(Modal.taskToRemove).equals(task);
        o(Modal.modals.removetask.isOpen).equals(true);
    });

    o('start editing a comment', function() {
        var comment = Mock().comments[0];
        Boards.beginEditComment(comment);

        o(Modal.commentEdit.id).equals(comment.id);
    });

    o('open remove comment modal', function() {
        var comment = Mock().comments[0];
        Boards.confirmRemoveComment(comment);

        o(Modal.commentToRemove.id).equals(comment.id);
        o(Modal.modals.removecomment.isOpen).equals(true);
    });
});
