var o = require('mithril/ospec/ospec');
var storageMock = require('./storagemock.js');

global.window = require('mithril/test-utils/browserMock.js')();
global.document = window.document;

window.localStorage = storageMock();
window.sessionStorage = storageMock();

var m = require('mithril');

var Request = require('../src/utils/request.js');

/*
o.spec('Request', function() {
    o('adds Authorization header when JWT is present', function() {
        return Request.post(Request.DOMAIN).then(function(response) {
            o(!!response).equals(true);
        });
    });
});
*/
