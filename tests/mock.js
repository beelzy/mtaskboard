var Mock = function() {
    var user = {id: '1', user_id: '1', board_id: '1'};
    var sharedUser = {
        default_board_id: '1',
        email: '',
        id: '1',
        last_login: '1560019072',
        security_level: '1',
        user_option_id: '1',
        username: 'user'
    };
    var categories = [
        {
            id: '1',
            name: 'Category 1',
            board_id: '1',
            default_task_color: '#ffffe0',
            ownCategory_task: [
                {category_id: '1',
                    id: '1',
                    task_id: '1'}
            ],
            sharedTask: [{
                id: '1',
                title: 'Title',
                color: '#ffffe0',
                column_id: '1',
                description: 'Description',
                due_date: '',
                id: '1',
                points: '0',
                position: '1',
            }]
        },
        {
            id: '2',
            name: 'Category 2',
            board_id: '1',
            default_task_color: '#ffffe0',
            ownCategory_task: [
                {category_id: '2',
                    id: '2',
                    task_id: '2'}
            ],
            sharedTask: [{
                id: '1',
                title: 'Title',
                color: '#ffffe0',
                column_id: '1',
                description: 'Description',
                due_date: '',
                id: '1',
                points: '0',
                position: '1',
            }]
        }
    ];
    var comments = [{
        id: '1',
        is_edited: '0',
        task_id: '1',
        text: 'Comment',
        timestamp: '1518594221036',
        user_id: '1'
    }, {
        id: '2',
        is_edited: '0',
        task_id: '1',
        text: 'Comment',
        timestamp: '1518594221036',
        user_id: '1'
    }];
    var tasks = [{
        color: '#ffffe0',
        column_id: '1',
        description: 'Task Description',
        due_date: '',
        id: '1',
        ownCategory_task: [{id: '1', category_id: '1', task_id: '1'}],
        ownComment: comments,
        points: '0',
        position: '1',
        sharedCategory: [{
            board_id: '1',
            default_task_color: '#ffffe0',
            id: '1',
            name: 'Category 1'
        }],
        title: 'Task Title',
        sharedUser: [sharedUser]
    }, {
        color: '#ffffe0',
        column_id: '1',
        description: 'Task Description',
        due_date: '1518594221036',
        id: '2',
        ownCategory_task: [{id: '2', category_id: '2', task_id: '2'}],
        points: '1',
        position: '2',
        sharedCategory: [{
            board_id: '1',
            default_task_color: '#ffffe0',
            id: '2',
            name: 'Category 2'
        }],
        title: 'Task Title'
    }
    ];
    var tasks2 = [{
        color: '#ffffe0',
        column_id: '2',
        description: 'Task Description',
        due_date: '',
        id: '3',
        ownCategory_task: [{id: '3', category_id: '1', task_id: '1'}],
        points: '0',
        position: '1',
        title: 'Task Title'
    }, {
        color: '#ffffe0',
        column_id: '2',
        description: 'Task Description',
        due_date: '',
        id: '4',
        ownCategory_task: [{id: '4', category_id: '2', task_id: '2'}],
        points: '0',
        position: '2',
        sharedCategory: [{
            board_id: '1',
            default_task_color: '#ffffe0',
            id: '2',
            name: 'Category 2'
        }],
        title: 'Task Title'
    }
    ];
    var columns = [
        {id: '1', name: 'Column 1', position: '3', board_id: '1', ownTask: tasks, task_limit: 0},
        {id: '2', name: 'Column 2', position: '2', board_id: '1', ownTask: tasks2, task_limit: 0}
    ];
    
    return {
        user: user,
        board: {
            id: '1',
            name: 'Test',
            is_active: '1',
            ownBoard_user: [user],
            ownCategory: categories,
            ownColumn: columns,
            sharedUser: [sharedUser]
        },
        columns: columns,
        tasks: tasks,
        tasks2: tasks2,
        categories: categories,
        comments: comments
    };
};

module.exports = Mock;
