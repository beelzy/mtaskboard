var o = require('mithril/ospec/ospec');

global.window = require('mithril/test-utils/browserMock.js');
global.document = window.document;

var Comment = require('../src/models/comment.js');
var User = require('../src/models/user.js');
var Mock = require('./mock.js');

o.spec('Comments', function() {
    o('checks if user can administrate comments', function() {
        User.updateUser(Mock().board.sharedUser[0]);
        var comment = Comment.fromData(Mock().comments[0]);

        o(Comment.canAdminComment(comment)).equals(true);

        User.user.id = 2;
        User.user.security_level = 3;
        o(Comment.canAdminComment(comment)).equals(false);
    });
    o('create an empty comment', function() {
        var comment = Comment.empty();

        o(comment.id).equals(0);
        o(comment.text).equals('');
        o(comment.user_id).equals(0);
        o(comment.task_id).equals(0);
        o(comment.timestamp).notEquals(undefined);
        o(comment.is_edited).equals(false);
    });
    o('create a comment from data', function() {
        var comment = Comment.fromData(Mock().comments[0]);

        o(comment.id).equals('1');
        o(comment.text).equals('Comment');
        o(comment.user_id).equals(1);
        o(comment.task_id).equals(1);
        o(comment.timestamp).equals(1518594221036);
        o(comment.is_edited).equals(false);
    });
});
