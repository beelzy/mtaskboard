var o = require('mithril/ospec/ospec');

global.window = require('mithril/test-utils/browserMock.js')();
global.documnet = window.document;

var User = require('../src/models/user.js');
var Mock = require('./mock.js');

o.spec('User', function() {
    o('updates the user', function() {
        User.updateUser(Mock().board.sharedUser[0]);
        o(Object.keys(User.user).length > 0).equals(true);
        o(User.user.id).equals(1);
        o(User.user.security_level).equals(1);
        o(User.user.user_option_id).equals(1);
        o(Object.keys(User.options).length).equals(0);
    });

    o('create user from data', function() {
        var user = User.fromData(Mock().board.sharedUser[0]);
        o(user.id).equals(1);
        o(user.security_level).equals(1);
        o(user.user_option_id).equals(1);
        o(user.username).equals('user');
    });

    o('checks if the user is an admin', function() {
        var user = Mock().board.sharedUser[0];
        User.updateUser(user);

        o(User.isAnyAdmin()).equals(true);
        user.security_level = 3;

        o(User.isAnyAdmin()).equals(false);
    });
});
